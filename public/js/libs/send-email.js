var m = new mandrill.Mandrill('eu5MQIijJFBJ2DUhBGns2w');
//mandril es un api que permite el envio de correos, esta api necesita una clave unica para cada aplicacion que la use
//la de arriba es la mia, o sea esto esta listo para ser usado

//aqui recibo Forma, pero como estamos usando angular no es necesario, puedes pasarle nada mas el nick que el usuario
//coloca en un input y ya, con eso buscas en la bdd y consigues el correo para enviarle la clave
function sendTheMail(email,contrasena)
{
        var asunto = "Recuperacion de Contraseña en Elements";

        var correo=email;

        var texto_correo = "Hola, esta es tu contraseña provicional de Elements "+contrasena;

        var params =
                {
                    "message":
                            {
                                "from_email": "passrecovery@elements.com",
                                "to": [{"email": correo}],
                                "subject": asunto,
                                "html": texto_correo,//aqui le dices si es html o text, en este caso yo use html
                                "autotext": "true"
                            }
                };
                
        if (email==contrasena)
            {
                alert("El usuario no se encontro en nuestra base de datos");
                return false;
            }
        m.messages.send(params, function(res)
        {
            alert("Se ha enviado tu password a tu correo");
            // parent.show();
        }, function(err)
        {
            alert("Hemos tenido problemas para procesar tu solicitud");
          //parent.show();
        });
    return false;
}