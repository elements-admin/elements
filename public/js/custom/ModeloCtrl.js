var app = angular.module('elements', ['alguna libreria que uses por ejemplo toaster si no usas ninguna solo se coloca []']);

//no cambien "elements" ese es el nombre de toda la app
app.controller('ModeloCtrl', function($scope,NombreFactory,toaster)//los factorys y librerias utilizadas se tiene que pasar por parametro al controlador para poder usarlo 
{
    NombreFactory.alguna_funcion().success(function(data)
                    {
                        //en "data" viene lo que estas retornando en tu funcion
                    });
                    
    NombreFactory.otra_funcion(8).success(function(data)
                    {
                        //esto devuelve 9
                    });
});

//un factory es como una clase estatica con metodos estaticos, los cuales puedes 
//acceder desde cualquier controlador siempre y cuendo lo pases por parametro
//puede tener 'n' funciones, y es genial xD
app.factory('NombreFactory', function($http) {
   
   return {
       alguna_funcion: function()
       {
           var algo=1;
           return algo;
       },
        otra_funcion: function(puedorecibirvariables)
       {
           return puedorecibirvariables+1;
       }
   }
    
});

//OJO: TODOS LOS CONTROLADORES, FACTORYS, DIRECTIVAS, SERVICIOS Y DEMAS VIENEN-SE DECLARAN
//EN EL MODULO QUE ESTAMOS USANDO, O SEA EN 'elements', ES DECIR EN LA VARIABLE APP