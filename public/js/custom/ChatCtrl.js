app.controller('ChatCtrl', function(Socket,$rootScope,$scope,Carta,InfoUser) 
{
	var audio = new Audio('http://www.innovandroid.com.ve/web/cartas_elements/ping.mp3');

	$scope.mensaje="";
	$scope.boton_chat=function()
	{
		Socket.emit('enviar_mensaje',{name:$scope.nombre, message:$scope.mensaje});
		$scope.mensaje="";
	};
	
	$scope.reiniciar=function()
	{
		$scope.nro_mensajes=0;
	};
	
	$rootScope.$on('set_nombre', function(event, data)
	{
		$scope.nombre=$rootScope.nombre;
	});
	$scope.nro_mensajes=0;
	$scope.messages=[];
	Socket.on('actualizar_historial',function(data)
	{
		$scope.messages=data;
		$scope.nro_mensajes++;
		audio.play();
	});
	
/////////////////////////INDIVIDUAL//////////////////////////
	$rootScope.$on('set_datos', function(event, data)
	{
		$scope.nombre=$rootScope.datos.nombre;
		$scope.room=$rootScope.datos.room;
	});
	
	$scope.boton_enviar=function()
	{
		Socket.emit('enviar_mensaje_individual',{name:$scope.nombre, message:$scope.mensaje,room:$scope.room});
		$scope.mensaje="";
	};
	$scope.messages_ind=[];
	Socket.on('recibir_mensaje',function(data)
	{
		$scope.messages_ind.push(data);
		audio.play();
		$scope.nro_mensajes++;
	});
	
});