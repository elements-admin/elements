var app = angular.module('elements', ['toaster','ngDraggable','ngAnimate']);

app.factory('Carta', function($http) 
{
    return {
        all: function()
        {
            return $http.get('getCartas');
        },
        mazo: function()
        {
            return $http.get('getMazo');
        },
        mazojuego: function(){
          
          return $http.get('getMazoJuego');  
        },
        almacen: function()
        {
            return $http.get('getAlmacen');
        },
        
        mons: function(){
            
            return $http.get('getMonstruo');
            
        },
         magicas: function(){
            
            return $http.get('getMagicas');
            
        },
       
     	update: function(cartas)
        {
            return $http.get('enviarAlmacen/'+cartas);
        },
        updatemaz: function(cartas)
        {
            return $http.get('enviarMazo/'+cartas);
        },
         comprarCartas: function(cartas)
        {
            return $http.get('comprarCartas/'+cartas);
        }
    };
});

app.factory('InfoUser', function($http) {
   
   return {
       
       get: function()
       {
           return $http.get('getInfoUser');
       },
       getCorreo: function(nick)
       {
        return $http.get('getCorreo/'+nick);
       },
       getClave: function(nick)
       {
        return $http.get('getClave/'+nick);
       },
	   actPerfil: function(bandera){
		   return $http.post('actPerfil/'+bandera);
	   }
   };
});

app.factory('Partida', function($http){
	
	return {
		
		post: function(jug2)
		{
			return $http.get('insertarPartida/'+jug2);
			
		}
		
	};
	
	
	
});

app.factory('Elemento', function($http) {
   
   return {
       
       get: function()
       {
           return $http.get('getElementos');
       }
   };
});

app.factory('Actividad', function($http) {
    
   
   return {
       
       
       get: function(){
           
           
           return $http.get('getActividad');
           
       },
	   
	   act: function(msj){
	   
		 
	     return $http.post('insActividad/'+msj);
		   
	   
   }
       
   };
    
});

app.factory('getMail', function($http) {
    
   
   return {
       
       
       get: function(){
           
           
           return $http.get('getCorreo');
           
       }
       
   };
    
});

app.factory('getContrasena', function($http) {
    
   
   return {
       get: function()
	   {
           return $http.get('getClave');
       }
   };
    
});

app.directive('ngEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if(event.which === 13) {
                    scope.$apply(function(){
                        scope.$eval(attrs.ngEnter, {'event': event});
                    });

                    event.preventDefault();
                }
            });
        };
    });


app.factory('Socket', function ($rootScope) 
{
	//var hostname="port-3000.jn0s1my0k7kqpvi28njdyjtsjdcxrcxqkp42i3qwoecdi.box.codeanywhere.com";
 // var baseURL           = location.protocol + "//" + hostname + ":" + location.port; // Call function to determine it
  
	//var socketIOPort          = 3000;
  //var socketIOLocation      = baseURL + socketIOPort; // Build Socket.IO location
  var socketIOLocation="http://port-3000.jn0s1my0k7kqpvi28njdyjtsjdcxrcxqkp42i3qwoecdi.box.codeanywhere.com";
//	alert(socketIOLocation);
	var socket = io.connect(socketIOLocation);

      
  return {
    on: function (eventName, callback) {
      socket.on(eventName, function () {  
        var args = arguments;
        $rootScope.$apply(function () {
          callback.apply(socket, args);
        });
      });
    },
    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      });
    }
  };
	
});
