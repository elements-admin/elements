app.controller('GameCtrl', function(Partida,Actividad,$rootScope,Socket,$scope,Carta,InfoUser,toaster)
{
	$scope.datosOponente=datosOponente;
    $scope.i = 0;
    $scope.url = "http://www.innovandroid.com.ve/web";
    $scope.urlT = $scope.url + "/cartas_elements/transparent.png"; //transparente
    $scope.urlB = $scope.url + "/cartas_elements/carta_reverso.jpg"; //trasera
    $scope.punto = 0;
	$scope.puntosOp = 0;
	$scope.barraOp=0;
	$scope.barra=0;
    $scope.exp = 0;
    $scope.detalle = $scope.urlB;
    $scope.texto = "Aqui va el detalle, todo se traera de la bdd y se mostrara aca, menos la imagen completa, que se mostrara al dar doble click";
    $scope.tuturno=true;
	$scope.exceso={fuego:0,agua:0,tierra:0,aire:0};
	var control = 1;
	var cartaAtaque;
	var indice;
	
	$scope.logoOp="http://www.innovandroid.com.ve/web"+datosOponente.ruta_logo;
	InfoUser.get().success(function(data)
    {
		 $scope.datos=data[0];
         $scope.logo="http://www.innovandroid.com.ve/web"+data[0].ruta_logo;

		 Carta.mazojuego().success(function(data)
      	 {
			Socket.emit('join_game',{yo:$scope.datos,oponente:$scope.datosOponente});
			$scope.mazo=data;
			 
			$scope.enMano=[];
			var i;
			for(i=0;i<5;i++)
			{
				$scope.enMano.push(data[0]);
				data.splice(0, 1);
			}
			
     	});
     });    
	////////////////////////VARIABLES DE OPONENTE/////////////////////////////
	var body = angular.element(document).find('body');
                    
    var url   = $scope.url;
    var urlB  = $scope.urlB;
    var urlT  = $scope.urlT;
    var punto = $scope.punto;
    var exp   = $scope.exp;
    var drops=0;
    var rutaTransparente="/cartas_elements/transparent.png"; //transparente
    var rutaReverso= "/cartas_elements/carta_reverso.jpg"; //trasera
	
	Socket.on('actualizar_mano_oponente',function(data)
	{
		$scope.enManoOp=data;
	});
		Socket.on('actualizar_mano',function(data)
	{
		$scope.enMano=data;
	});
		Socket.on('actualizar_excesos',function(data)
	{
		$scope.exceso=data;
	});
	Socket.on('actualizar_m_oponente',function(data)
	{
		$scope.mOp=data;
	});
	Socket.on('actualizar_mg_oponente',function(data)
	{
		$scope.mgOp=data;
	});
	Socket.on('actualizar_mg_oponente',function(data)
	{
		$scope.mgOp=data;
	});
	Socket.on('actualizar_background',function(data)
	{
		cambiarFondo(data);
	});
	Socket.on('actualizar_puntos_oponente',function(data)
	{
		$scope.puntosOp=data;
		$scope.barraOp=getValorBarra($scope.puntosOp);
	});
		Socket.on('actualizar_puntos',function(data)
	{
		$scope.punto=data;
		$scope.barra=getValorBarra($scope.punto);
	});
	Socket.on('actualizar_cementerio_oponente',function(data)
	{
		$scope.cOp = data;
	});
		Socket.on('actualizar_cementerio',function(data){
		
		$scope.cTu = data;
	});
	
	Socket.on('actualizar_m',function(data){
		
	$scope.mTu = data;
	});
	
	Socket.on('actualizar_mazo_oponente',function(data){
		$scope.mazoOp = data;
	});
	
	Socket.on('actualizar_perdedor',function(data){
		
		derrota(data);
		
	});
	
	Socket.on('activarme',function()
	{
		setEsTurno(true);
		mandarCementerioMagicas();
	});
	
	Socket.on('solicitud_mazo',function()
	{
		Socket.emit('send_mazo',{mazo:$scope.mazo,room:$scope.room});
	});
	
	
	Socket.on('set_room',function(data)
	{
		$scope.room=data.room;
		$rootScope.datos={nombre:$scope.datos.user,room:$scope.room};
		$rootScope.$emit('set_datos');
		Socket.emit('send_mano',{mano:$scope.enMano,room:$scope.room});
		Socket.emit('send_mazo',{mazo:$scope.mazo,room:$scope.room});
		Socket.emit('get_mazo',{room:$scope.room});
		switch(data.rol)
		{
			case 1:cambiarFondo($scope.datos.id_elemento);
				Socket.emit('send_background',{background:$scope.datos.id_elemento,room:$scope.room});
				//setEsTurno(false);
				$scope.tuturno=false;
				break;
			case 2:cambiarFondo($scope.datosOponente.id_elemento);
				Socket.emit('send_background',{background:$scope.datosOponente.id_elemento,room:$scope.room});
				$scope.tuturno=true;
				break;
		}
	});
		
	$scope.isTransparent=function(img)
	{
		if(img.ruta===rutaTransparente)
		{
			return true;
		}
		else return false;
	};
	
	function ataque_directo(index)
	{
		
		var exceso=getExceso($scope.mTu[index].elemento);
		$scope.punto += parseInt($scope.mTu[index].ataque) + exceso;
		$scope.barra = getValorBarra($scope.punto);
		Socket.emit('send_puntos',{puntos: $scope.punto,room:$scope.room});
		toaster.pop("success", "","Ataque Directo!", 500, 'trustedHtml');
		setEsTurno(false);
	}
	
	function ataque_indirecto()
	{if(!$scope.tuturno){return false;}
	 
		var i = 0;
		$scope.mOp.forEach(function(carta) 
		{
    		if(carta.ruta != rutaTransparente)
			{
			    var id = document.getElementById(""+carta.id);
			     angular.element(id).addClass('bordeRojo');		
		    }
		});
	}
	
	function mandarCementerio(index)
	{
		  var elem = document.getElementById(""+index);
		angular.element(elem).addClass('explotar');	
		
		$scope.cOp.push($scope.mOp[index]);
		$scope.mOp[index]= {ruta: rutaTransparente};
		Socket.emit('send_cementerio_oponente',{cementerio: $scope.cOp, room: $scope.room});
		Socket.emit('send_m_oponente',{m:$scope.mOp, room:$scope.room});
	}
	
	function mandarCementerioMagicas()
	{
		$scope.mgTu.forEach(function(magica,index)
		{
			if(magica.ruta!==rutaTransparente)
			{
				$scope.cTu.push(magica);
				$scope.mgTu[index]= {ruta: rutaTransparente};
			}
		});
		
		Socket.emit('send_cementerio',{cementerio: $scope.cTu, room: $scope.room});
		Socket.emit('send_mg',{mg:$scope.mgTu, room:$scope.room});
	}
	
	function mandarCementerioP(index)
	{	
		$scope.cTu.push($scope.mTu[index]);
		$scope.mTu[index] = {ruta: rutaTransparente};
		
		Socket.emit('send_cementerio',{cementerio: $scope.cTu, room: $scope.room});
		Socket.emit('send_m',{m:$scope.mTu, room:$scope.room});
	}
	
	function mandarCementerioAmbos(ind,index)
	{
		$scope.cTu.push($scope.mTu[ind]);
		$scope.cOp.push($scope.mOp[index]);
		$scope.mTu[ind] = {ruta: rutaTransparente};
		$scope.mOp[index] = {ruta: rutaTransparente};
		Socket.emit('send_cementerio',{cementerio: $scope.cTu, room: $scope.room});
		Socket.emit('send_cementerio_oponente',{cementerio: $scope.cOp, room: $scope.room});
		Socket.emit('send_m_oponente',{m:$scope.mOp, room:$scope.room});
	    Socket.emit('send_m',{m:$scope.mTu, room:$scope.room});
	}
	
	$scope.recibe_ataque = function(index)
	{
			var aux=angular.element(document.getElementById(""+$scope.mOp[index].id));
			if(aux.hasClass('bordeRojo'))
			{
				var tu_ataque=parseInt(cartaAtaque.ataque);
				var ataque_oponente=parseInt($scope.mOp[index].ataque);
				if(tu_ataque > ataque_oponente)
				{
					$scope.punto += parseInt(cartaAtaque.ataque -$scope.mOp[index].ataque);
					$scope.barra = getValorBarra($scope.punto);
					Socket.emit('send_puntos',{puntos: $scope.punto,room:$scope.room});
					mandarCementerio(index);
				}
		
				if(tu_ataque < ataque_oponente)
				{
					$scope.puntosOp += parseInt($scope.mOp[index].ataque - cartaAtaque.ataque);
					$scope.mOp[index].ataque= ataque_oponente -tu_ataque;
					$scope.barraOp = getValorBarra($scope.puntosOp);
					Socket.emit('send_puntos_oponente',{puntos: $scope.puntosOp,room:$scope.room});
					Socket.emit('send_m_oponente',{m:$scope.mOp, room:$scope.room});
					mandarCementerioP(indice);
				}
				
				if(tu_ataque === ataque_oponente)
				{
					mandarCementerioAmbos(indice,index);
				Socket.emit('desactivarme',{room:$scope.room});	
				$scope.tuturno= false;
				toaster.pop("success", "","Ha terminado tu turno", 1000, 'trustedHtml');
			}
		
		$scope.mOp.forEach(function(carta)
		{
			var aux=angular.element(document.getElementById(""+carta.id));
			if(aux.hasClass('bordeRojo'))
			{
				aux.removeClass('bordeRojo');
			}
		});
				setEsTurno(false);
	};
	};
	
	$scope.atacar = function(index,data,$event)
	{
		
		var i = 0;
		var band = 0;
		var exceso = 0;
		var id = document.getElementById(""+$scope.mTu[index].id);
		var e=angular.element(id);
		$scope.mTu.forEach(function(carta)
		{
			var aux=angular.element(document.getElementById(""+carta.id));
			if(aux.hasClass('bordeVerde'))
			{
				aux.removeClass('bordeVerde');
			}
		});
		$scope.mOp.forEach(function(carta)
		{
			var aux=angular.element(document.getElementById(""+carta.id));
			if(aux.hasClass('bordeRojo'))
			{
				aux.removeClass('bordeRojo');
			}
		});
	    if($scope.mTu[index].ruta != rutaTransparente)
		{
			if(!esTurno()){return false;}
		       	while(i < 3)
				{
					if($scope.mOp[i].ruta != rutaTransparente){
					
						band = 1;
					    break;
					}
					i++;
				}
		
				e.addClass('bordeVerde');
			
			if(band === 0)
			{				
				ataque_directo(index);
			}
			
			else
			{
				cartaAtaque = $scope.mTu[index];
				indice = index;
				ataque_indirecto(index,$event);
			}
			
			if($scope.punto >= total)
			{
				$scope.tuturno = false;
				var vict ="Felicidades has vencido a "+$scope.datosOponente.user+"!";
			
				var msj = "Has sido derrotado por "+$scope.datos.user;
				Socket.emit('send_perdedor',{mensaje:msj, room:$scope.room});
			    Partida.post($scope.datosOponente.user).success(function(){});
				
				$scope.exp = 25/parseInt($scope.datos.nivel);
				$scope.oro = 500;
				
				Actividad.act(vict).success(function(data)
				{
				    $rootScope.$emit('send_actividad','data');	
					InfoUser.actPerfil(1).success(function(data)
					{
						$('#miventanaganador').modal({keyboard: false,backdrop: 'static'});  
						$('#miventanaganador').modal('show');
					});			
				});
			}
		}		
	};
	
	function derrota(mensaje)
	{
		
				$scope.exp = 25/parseInt($scope.datos.nivel);
				$scope.oro = 500/3;
		$scope.tuturno = false;
		$('#miventanaperdedor').modal({keyboard: false,backdrop: 'static'});  
		$('#miventanaperdedor').modal('show'); 
		Actividad.act(mensaje).success(function(data)
		{
				     $rootScope.$emit('send_actividad','data');	
		});
		InfoUser.actPerfil(0).success(function(data){});
	}
	
	$scope.home = function()
	{
		 open('GET', 'home',{});
	};
	
	  open = function(verb, url, data, target) 
  {
  var form = document.createElement("form");
  form.action = url;
  form.method = verb;
  form.target = target || "_self";
  if (data) {
    for (var key in data) {
      var input = document.createElement("textarea");
      input.name = key;
      input.value = typeof data[key] === "object" ? JSON.stringify(data[key]) : data[key];
      form.appendChild(input);
    }
  }
  form.style.display = 'none';
  document.body.appendChild(form);
  form.submit();
};
	
	
	$scope.tieneAtaque=function()
	{
	 	if(( $scope.imgDetalle.ataque!==undefined)&&( $scope.imgDetalle.ataque!=='0'))
		{
			return true;
		}return false;
	};
//Mostruos tuyos, iniciados en transparente
    $scope.mTu = [{ruta: rutaTransparente}, { ruta: rutaTransparente}, {ruta: rutaTransparente}]; 
	//Mostruos de oponente, iniciados en transparente
   $scope.mOp = [{ruta: rutaTransparente}, { ruta: rutaTransparente}, {ruta: rutaTransparente}];
//Magicas de oponente, iniciados en transparente
    $scope.mgOp =  [{ruta: rutaTransparente}, {ruta: rutaTransparente }]; 
    //Magicas tuyas, iniciados en transparente
	$scope.mgTu = [{ruta: rutaTransparente}, {ruta: rutaTransparente }]; 
//en mano de oponente 	
    $scope.enManoOp = [{ruta: rutaReverso}, {ruta: rutaReverso },{ruta: rutaReverso}, {ruta: rutaReverso },{ruta: rutaReverso}]; 
    //cementerio tuyo
	$scope.cTu = [];
	//cementerio de oponente
	$scope.cOp = [];
	$scope.invocar_restrict=true;
  var fondoAgua={background:'url(http://www.innovandroid.com.ve/web/cartas_elements/Tableros/TABLERO_AGUA.jpg)100% 100% fixed'};
  var fondoFuego={background:'url(http://www.innovandroid.com.ve/web/cartas_elements/Tableros/TABLERO_FUEGO.jpg) 100% 100% fixed'};
  var fondoAire={background:'url(http://www.innovandroid.com.ve/web/cartas_elements/Tableros/TABLERO_AIRE.JPG)100% 100% fixed'};
  var fondoTierra={background:'url(http://www.innovandroid.com.ve/web/cartas_elements/Tableros/TABLERO_TIERRA.JPG)100% 100% fixed'};
      
	function esTurno()
	{
		if(!$scope.tuturno)
		{
			toaster.pop("error", "","Debes esperar tu turno para jugar", 500, 'trustedHtml');
			return false;
		}else return true;
	}
	
	function setEsTurno(sino)
	{
		$scope.tuturno=sino;
		switch(sino)
		{
			case false:
				{
					drops=0;
					Socket.emit('desactivarme',{room:$scope.room});	
					toaster.pop("success", "","Ha terminado tu turno", 1000, 'trustedHtml');
					break;
				}
			case true:
				{
					toaster.pop("success", "","Ahora puedes jugar", 1000, 'trustedHtml');break;
				}
		}
	}
	
    $scope.onDropCompleteM = function(index, data, e) 
    {
		if(!esTurno()){return false;}
		
		if(drops>1)
		{
			toaster.pop("note", "","Ya has invocado por este turno", 500, 'trustedHtml');
			return false;
		}
		//cambiarFondo(data.elemento);
		//Socket.emit('send_background',{background:data.elemento,room:$scope.room});
		if (sacrificio(index,data)&&(data.elemento != 5)&&(vaciarMano(data)))
		{
			drops++;
			$scope.mTu[index] =  data;
			Socket.emit('send_m',{m:$scope.mTu,room:$scope.room});
			Socket.emit('send_mano',{mano:$scope.enMano,room:$scope.room});
		}
    };

    $scope.onDropCompleteMg = function(index, data, evt) 
	{
		if(!esTurno()){return false;}
		if(drops>1)
		{
			toaster.pop("note", "","Ya has invocado por este turno", 500, 'trustedHtml');
			
			return false;
		}
        if (($scope.mgTu[index].ruta == rutaTransparente)&&(data.elemento == 5)&&(vaciarMano(data)))
        {
			drops++;
			efectoMagica(data);
            $scope.mgTu[index] = data;
			Socket.emit('send_mg',{mg:$scope.mgTu,room:$scope.room});
			Socket.emit('send_mano',{mano:$scope.enMano,room:$scope.room});
			if(drops==2) setEsTurno(false);
        }
    };
	
	function sacrificio(index,carta_actual)
	{
		if($scope.invocar_restrict===false)
		{
			toaster.pop("note", "","has invocado correctamente (Carta magica permite condicion)", 1000, 'trustedHtml');
			setInvocarRestrict(true);
			return true;
		}
		if(((($scope.mTu[index].nivel)===undefined)&&(parseInt(carta_actual.nivel)==1))||((($scope.mTu[index].nivel)===undefined)&&(parseInt(carta_actual.nivel)==2)))
		{
			toaster.pop("note", "","has invocado correctamente", 1000, 'trustedHtml');
			return true;
		}
		
		if((parseInt($scope.mTu[index].nivel)==(parseInt(carta_actual.nivel)-1))||(parseInt($scope.mTu[index].nivel)==(parseInt(carta_actual.nivel))))        
		{
			toaster.pop("note", "","has invocado correctamente", 1000, 'trustedHtml');
			return true;
		}
	
			
			
		toaster.pop("note", "","no se puede realizar invocacion", 1000, 'trustedHtml');
		return false;
	}
    
       $scope.onDropCompleteMazo = function(index, data, evt) 
       {
		   if(!esTurno()){return false;}
		   //if(!$scope.tuturno){return;}
           if(($scope.enMano[index].ruta == rutaTransparente)&&(vaciarMano(data)))
            {
                 $scope.enMano[index] =data;
                 eliminarDeMazo(data);
            }
		   else if(($scope.enMano[index].ruta == rutaTransparente))
            {
                  $scope.enMano[index] =data;
                 eliminarDeMazo(data);
            }
		    Socket.emit('send_mano',{mano:$scope.enMano,room:$scope.room});
            //alert($scope.enMano[index].url+" por "+$scope.url+data.ruta);
       };
	
$scope.ataque_restante=0;
    $scope.showDetalle = function(img)
    {
            $scope.detalle = url+img.ruta;
		 $scope.imgDetalle=img;    
		// alert(img.elemento+" "+img.ataque+" y "+getExceso(img.elemento));
			$scope.ataque_restante=parseInt(img.ataque)+getExceso(img.elemento);
    };
	
	function getExceso(elemento)
	{
		switch(elemento)
		{
			case '3':return $scope.exceso.aire;
			case '4':return $scope.exceso.fuego;
			case '1':return $scope.exceso.agua;
			case '2':return $scope.exceso.tierra;
			default: return 0;
		}
	}

    function vaciarMano(data)
    {
        var index = $scope.enMano.indexOf(data);
        if (index > -1)
        {
             $scope.enMano[index]={ruta:rutaTransparente};
            return true;
        }
        return false;
    }

    
        function eliminarDeMazo(data) 
        {
        var index = $scope.mazo.indexOf(data);
        if (index > -1) {
            $scope.mazo.splice(index, 1);
        }
    }
	
	//////////////////////////////////////FONDOS////////////////////////////////////
	var total=2000;
	
function getValorBarra(puntos)
{
	return (puntos*100)/total;
}

	//////////////////////////////////EFECTOS MAGICAS///////////////////////////////////
	
	function efectoMagica(carta)
	{
		switch(carta.especial)
		{
			case '1':break;
			case '2':afectarPuntosVida(100,'mix');break;
			case '3':afectaAtaque(30,'a','fuego');break;
			case '4':afectaAtaque(25,'d','fuego');break;
			case '5':afectaAtaque(30,'a','agua');break;
			case '6':afectaAtaque(25,'d','agua');break;
			case '7':afectaAtaque(25,'a','tierra');break;
			case '8':afectaAtaque(25,'d','tierra');break;
			case '9':afectaAtaque(25,'a','aire');break;
			case '10':afectaAtaque(25,'d','aire');break;
			case '11':
				Socket.emit('send_background',{background:'4',room:$scope.room});
				cambiarFondo('4');break;//fuego
			case '12':
				Socket.emit('send_background',{background:'1',room:$scope.room});
				cambiarFondo('1');break;//agua
			case '13':
				Socket.emit('send_background',{background:'2',room:$scope.room});
				cambiarFondo('2');break;//tierra
			case '14':
				Socket.emit('send_background',{background:'3',room:$scope.room});
				cambiarFondo('3');break;//aire
			case '15':afectaAtaque(20,'a','all');break;
			case '16':afectaAtaque(15,'d','all');break;
			case '17':revivirUltimo();break;
			case '18':break;
			case '19':break;
			case '20':break;
			case '21':break;
			case '22':destruirCarta('debil');break;
			case '23':destruirCarta('magica');break;
			case '24':destruirCarta('mano');break;
			case '25':break;
			case '26':break;
			case '27':break;
			case '28':break;
			case '29':setInvocarRestrict(false);break;//para cartas de 2 a 4 estrellas
			case '30':break;
			case '31':break;
			case '32':break;
			case '33':destruirCarta("todas_magicas");break;
			case '34':break;
			case '35':cambiarFondo('rand');break;
			case '36':afectarPuntosVida(20,'agua');break;
			case '37':afectarPuntosVida(20,'aire');break;
			case '38':afectarPuntosVida(20,'tierra');break;
			case '39':afectarPuntosVida(20,'fuego');break;
			case '40':break;
			default:alert("ERROR NO MAGICA");
		}
	}
	
	
	function cambiarFondo(data)
	{
		if(data==='rand')
		{
			var aux=Math.floor((Math.random() * 4) + 1);
			data=+aux+'';
			Socket.emit('send_background',{background:data,room:$scope.room});
		}
		switch(data)
		{
			case '2':body.css(fondoTierra);break;
			case '4':body.css(fondoFuego);break;
			case '3':body.css(fondoAire);break;
			case '1':body.css(fondoAgua);break;
		}
	};
	
		function revivirUltimo()
	{
		$scope.mTu.forEach(function(carta,index)
		{
    		if((carta.ruta===rutaTransparente)&&($scope.cTu[$scope.cTu.length-1].elemento!=='5'))
			{
				$scope.mTu[index]=$scope.cTu[$scope.cTu.length-1];
				$scope.cTu.pop();
				Socket.emit('send_cementerio',{cementerio: $scope.cTu, room: $scope.room});
			    Socket.emit('send_m',{m:$scope.mTu, room:$scope.room});
				return true;
			}
		});
		
	};

	
	function afectarPuntosVida(puntos,data)
	{
		switch(data)
		{
			case 'mix':
				$scope.punto+=puntos;
				$scope.puntosOp-=puntos;
				break;
			case 'fuego':$scope.puntosOp-=puntos*getNumeroEnCampo('4');break;
			case 'agua':$scope.puntosOp-=puntos*getNumeroEnCampo('1');break;
			case 'aire':$scope.puntosOp-=puntos*getNumeroEnCampo('3');break;
			case 'tierra':$scope.puntosOp-=puntos*getNumeroEnCampo('2');break;
		}
		$scope.barra = getValorBarra($scope.punto);
		$scope.barraOp= getValorBarra($scope.puntosOp);
		Socket.emit('send_puntos',{puntos: $scope.punto,room:$scope.room});
		Socket.emit('send_puntos_oponente',{puntos: $scope.puntosOp,room:$scope.room});
	};
	
	
	function getNumeroEnCampo(data)
	{
		var aux=0;
		$scope.mOp.forEach(function(carta)
		{
    		if(carta.elemento===data)
			{
				aux++;
			}
		});
			$scope.mTu.forEach(function(carta) {
    		if(carta.elemento===data)
			{
				aux++;
			}
		});
		return aux;
	}
	
	function getNumeroCartas(data)
	{
		var aux=0;
		
		$scope.enManoOp.forEach(function(carta)
		{
    		if(carta.elemento===data)
			{
				aux++;
			}
		});
			$scope.enMano.forEach(function(carta) {
    		if(carta.elemento===data)
			{
				aux++;
			}
		});
		return aux;
	}
	
	function afectaAtaque(puntos,tipo,elemento)
	{
		switch(tipo)
		{
			case 'a':aumentaAtaque(puntos,elemento);break;
			case 'd':disminuirAtaque(puntos,elemento);break;
		}
		
	};
		
	function aumentaAtaque(puntos,elemento)
	{
		switch(elemento)
		{
			case 'fuego':$scope.exceso.fuego+=puntos;break;
			case 'aire':$scope.exceso.aire+=puntos;break;
			case 'agua':$scope.exceso.agua+=puntos;break;
			case 'tierra':$scope.exceso.tierra+=puntos;break;
			case 'all':aumentarTodos(puntos);
		}
		Socket.emit('send_excesos',{excesos:$scope.exceso, room:$scope.room});
		
	};
	
	function aumentarTodos(puntos)
	{
		$scope.mTu.forEach(function(carta,index)
		{
			
			if(carta.ruta!=rutaTransparente)
			{
				$scope.mTu[index].ataque=(parseInt($scope.mTu[index].ataque)+puntos);
			}
		});
		Socket.emit('send_m',{m:$scope.mTu, room:$scope.room});
	}
		function disminuirTodos(puntos)
	{
		$scope.mOp.forEach(function(carta,index)
		{
			if(carta.ruta!=rutaTransparente)
			{
				$scope.mOp[index].ataque=(parseInt($scope.mOp[index].ataque)-puntos);
			}
		});
		Socket.emit('send_m_oponente',{m:$scope.mOp, room:$scope.room});
	}
	
	function disminuirAtaque(puntos,elemento)
	{
		switch(elemento)
		{
			case 'fuego':$scope.exceso.fuego-=puntos;
				break;
			case 'aire':$scope.exceso.aire-=puntos;break;
			case 'agua':$scope.exceso.agua-=puntos;break;
			case 'tierra':$scope.exceso.tierra-=puntos;break;
				case 'all':disminuirTodos(puntos);
		}
		Socket.emit('send_excesos',{excesos:$scope.exceso, room:$scope.room});
	}
	
	function destruirCarta(data)
	{
		switch(data)
		{
			case 'debil':destruirMasDebil();break;
			//case 'magica':$scope.excesoAire-=puntos;break;
			case 'mano':destruirDeMano();break;
			//case 'todas_magicas':$scope.excesoTierra-=puntos;break;
		}
	}
	
	function destruirMasDebil()
	{
		var aux=0;
		var masdebil=parseInt($scope.mOp[0].ataque);
			$scope.mOp.forEach(function(carta,index) 
		    {
				if(carta.ruta!=rutaTransparente)
				{
					var ataque=parseInt(carta.ataque);
					if(ataque<masdebil)
					{
						masdebil=ataque;
						aux=index;
					}
				}
		});
		mandarCementerio(aux);
	}
	
	function destruirDeMano()
	{
		var aux=Math.floor((Math.random() * 4));
		$scope.enManoOp[aux]={ruta:rutaTransparente};
		Socket.emit('send_mano_oponente',{mano:$scope.enManoOp,room:$scope.room});
	}

	function setInvocarRestrict(data)
	{
		$scope.invocar_restrict=data;
	};
	
	////////////////////////////////////////////pasara turno y salir//////////////////////7
	$scope.pasar_button=function()
	{
		if(!esTurno()){return false;}
		setEsTurno(false);
	};
	
/*function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}*/
	
});