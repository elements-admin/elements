app.controller('SesionCtrl', function($rootScope,$scope,toaster,InfoUser,Socket,Actividad) 
{
    InfoUser.get().success(function(data)
    {
          $scope.datos=data[0];
		  Socket.emit('conectado',$scope.datos.user);
			$rootScope.nombre=$scope.datos.user;
		  $rootScope.$emit('set_nombre');
     });
	$scope.noSoyYo=function(user)
	{
		if($scope.datos.user!==user)
		{
			return true;
		}else return false;
	};
	Socket.on('actualizar_nro_conectados',function(data)
	{
		$scope.nro_conectados=data;
	});
	
	Socket.on('actualizar_conectados',function(data)
	{
		$scope.conectados=data;
	});
	
	$scope.invitarUser=function(data)
	{
		Socket.emit('invitacion_individual',{'quien_invita':$scope.datos,'invitado':data});
	};
	
	$scope.invitarTodos=function(data)
	{
		Socket.emit('invitacion_abierta',$scope.datos);
	};
	
	Socket.on('fui_invitado',function(data)
	{
		$scope.dataOponente=data;
		var post=data.user+" te ha invitado a jugar";
		var post1 = "Fuiste invitado a jugar por "+data.user;
		
		Actividad.act(post1).success(function(data){
			
		
			
			$rootScope.$emit('send_actividad','data');
			
			
		});
		
		$rootScope.msg=post;
		toaster.pop("note", "",post, 100000, 'trustedHtml');
		
		
	});
	
	$rootScope.$on('aJugar', function(event, data)
	{
		aJugar($scope.dataOponente,$scope.datos,true);
	});
	
    Socket.on('aJugar',function(data)
	{
		 aJugar(data,$scope.datos,false);
	});
	
	aJugar=function(oponente,yo,host)
	{
		var datosOponente=angular.toJson(oponente);
		if(host)
		 Socket.emit('respuesta',{'quien_invita':oponente,'yo':yo});
		 open('POST', 'game', {'datosOponente':datosOponente});
	};
	
	
  open = function(verb, url, data, target) 
  {
  var form = document.createElement("form");
  form.action = url;
  form.method = verb;
  form.target = target || "_self";
  if (data) {
    for (var key in data) {
      var input = document.createElement("textarea");
      input.name = key;
      input.value = typeof data[key] === "object" ? JSON.stringify(data[key]) : data[key];
      form.appendChild(input);
    }
  }
  form.style.display = 'none';
  document.body.appendChild(form);
  form.submit();
};
	
	
});