app.controller('RegistroCtrl', function($scope,Elemento,InfoUser)
{
    
 
    
    Elemento.get().success(function(data)
    {
        $scope.elementos = data;
    });
    
    $scope.element="Seleccionar";
    $scope.idElement;
    $scope.nick;
    $scope.correo;
    $scope.contrasena;
    
    $scope.enviarCorreo = function (){
       
        InfoUser.getCorreo($scope.nick).success(function(data)
        {
            $scope.correo = data;
            InfoUser.getClave($scope.nick).success(function(data)
            {
                $scope.contrasena = data;
                sendTheMail($scope.correo,$scope.contrasena);
            });
            
        }
        
        );
    };
    
    $scope.myFunction = function (){
        
        if (document.getElementById('1').checked) {
            $scope.idElement = "1";
            $scope.element=document.getElementById('1').value;
        }
        
        if (document.getElementById('2').checked) {
            $scope.idElement = "2";
            $scope.element=document.getElementById('2').value;
        }
        
        if (document.getElementById('3').checked) {
            $scope.idElement = "3";
            $scope.element=document.getElementById('3').value;
        }
        
        if (document.getElementById('4').checked) {
            $scope.idElement = "4";
            $scope.element=document.getElementById('4').value;
        }
        
    };
    
});