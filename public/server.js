var express = require('express');
var http = require('http');
var app = express();
var server = require('http').Server(app);
var server = app.listen(3000, '0.0.0.0', function()
{
  console.log('Listening on port %d', server.address().port);
});

var io = require('socket.io').listen(server);
var visits=0;
var lista_conectados=[];
var lista_sockets=[];
var rooms=[];
var historial=[];

io.on('connection', function (socket)
{
///////////////////////////////////SESION/////////////////////////////////////////////////
  socket.on('conectado', function (data)
  {
	visits++;
	io.sockets.emit('actualizar_nro_conectados',visits);
	lista_conectados.push(data);
	io.sockets.emit('actualizar_conectados',lista_conectados);
	lista_sockets.push(socket);
    console.log(data+" se logueo");
  });
	
  socket.on('disconnect', function ()
  {
	  var i=lista_sockets.indexOf(socket);
	  if(lista_conectados[i]!==undefined)
	  {
		  visits--;
			io.sockets.emit('actualizar_nro_conectados',visits);
			console.log(lista_conectados[i]+'Got disconnect!');
			lista_conectados.splice(i,1);
		  	//rooms.splice(i,1);
			io.sockets.emit('actualizar_conectados',lista_conectados);
			lista_sockets.splice(i,1);
	  }
  });
///////////////////////////INVITACIONES/////////////////////////////////////////////////
  socket.on('invitacion_individual', function (data)
  {
	  var i=lista_conectados.indexOf(data.invitado);
	  if(lista_sockets[i]!==undefined)
	  {
		  io.sockets.connected[lista_sockets[i].id].emit('fui_invitado',data.quien_invita);
	  }
  });
	
  socket.on('invitacion_abierta', function (data)
  {
	 socket.broadcast.emit('fui_invitado',data);
  });
	
	socket.on('respuesta', function (data)
  {
	 var i=lista_conectados.indexOf(data.quien_invita.user);
	  if(lista_sockets[i]!==undefined)
	  {
		 io.sockets.connected[lista_sockets[i].id].emit('aJugar',data.yo);
	  }
  });
	

//////////////////////////CHAT GRUPAL///////////////////////////////////////////////////
	socket.on('enviar_mensaje',function(data)
	{
		historial.push(data);
		io.sockets.emit('actualizar_historial',historial);
	});

//////////////////////////CHAT INDIVIDUAL///////////////////////////////////////////////
	socket.on('enviar_mensaje_individual',function(data)
	{
		io.sockets.in(data.room).emit('recibir_mensaje',{name:data.name,message:data.message});
	});
//////////////////////////JUGANDO///////////////////////////////////////////////////////
  socket.on('join_game', function (data)
  {
	 var room1=data.yo.user+data.oponente.user;
	  var room2=data.oponente.user+data.yo.user;
	  var i =rooms.indexOf(room2);
	  if(rooms[i]!==undefined)
	  {
		  socket.join(room2);
		   console.log(room1+' y '+room2+" a jugar en "+room2);
		  io.sockets.connected[socket.id].emit('set_room',{room:room2,rol:2});
	  }
	  else
	  {
		  socket.join(room1);
		  rooms.push(room1);
		  io.sockets.connected[socket.id].emit('set_room',{room:room1,rol:1});
		  console.log(room1+' y '+room2+" a jugar en "+room1);
	  }
  });
	
   socket.on('send_mg', function (data)
  {
	   console.log(data.room+" enviando magicas");
	   socket.broadcast.to(data.room).emit('actualizar_mg_oponente',data.mg);
  });

	socket.on('send_mano', function (data)
  {
		 console.log(data.room+" enviando mano");
	  socket.broadcast.to(data.room).emit('actualizar_mano_oponente',data.mano);
		 //io.sockets.in(data.room).emit('actualizar_mano_oponente',data.mano);
  });
		socket.on('send_mano_oponente', function (data)
  {
		 console.log(data.room+" enviando mano oponente");
	  socket.broadcast.to(data.room).emit('actualizar_mano',data.mano);
		 //io.sockets.in(data.room).emit('actualizar_mano_oponente',data.mano);
  });
  
   socket.on('send_m', function (data)
  {
	  console.log(data.room+" enviando mostruos");
	  socket.broadcast.to(data.room).emit('actualizar_m_oponente',data.m);
  });

   socket.on('send_background', function (data)
  {
       console.log(data.room+" enviando background");
	  socket.broadcast.to(data.room).emit('actualizar_background',data.background);
  });
	
  socket.on('send_puntos', function (data)
  {
	  console.log(data.room+" enviando puntos");
	  socket.broadcast.to(data.room).emit('actualizar_puntos_oponente',data.puntos);
  });
	 socket.on('send_puntos_oponente', function (data)
  {
	  console.log(data.room+" enviando puntos");
	  socket.broadcast.to(data.room).emit('actualizar_puntos',data.puntos);
  });
	
	
	socket.on('send_cementerio_oponente', function (data)
	{
		console.log(data.room+" actualizando cementerio");
		socket.broadcast.to(data.room).emit('actualizar_cementerio_oponente',data.cementerio);
		
	});
		socket.on('send_cementerio', function (data){
		
		console.log(data.room+" actualizando cementerio");
		socket.broadcast.to(data.room).emit('actualizar_cementerio',data.cementerio);
		
	});
	
	socket.on('send_m_oponente', function (data){
		
		console.log(data.room+" actualizando mounstruos");
		socket.broadcast.to(data.room).emit('actualizar_m',data.m);
		
	});

	
	socket.on('send_mazo', function (data)
	{
		
		console.log(data.room+" actualizando mazo");
		socket.broadcast.to(data.room).emit('actualizar_mazo_oponente',data.mazo);
		
	});
		socket.on('send_excesos', function (data)
	{
		
		console.log(data.room+" actualizando excesos");
		socket.broadcast.to(data.room).emit('actualizar_excesos',data.excesos);
		
	});
	
	socket.on('get_mazo', function (data)
		{
		console.log(data.room+" solicitando mazo");
		socket.broadcast.to(data.room).emit('solicitud_mazo');
	});
		socket.on('desactivarme', function (data)
		{
		console.log(data.room+" desactivandome");
		socket.broadcast.to(data.room).emit('activarme');
		
	});
	
	   socket.on('send_perdedor', function (data){
		  
		   console.log(data.room+' enviando perdedor');
		   socket.broadcast.to(data.room).emit('actualizar_perdedor',data.mensaje);
		   
	   });
	
	
});