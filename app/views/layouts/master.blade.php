<!doctype html>
<html ng-app="elements">
<head>
    <meta charset="utf-8">
    <title>Elements</title>
    {{HTML::style('css/bootstrap.min.css')}}
    {{HTML::style('css/jumbotron-narrow.css')}}
    {{HTML::style('css/toaster.css')}}
    
    {{HTML::script('//ajax.googleapis.com/ajax/libs/angularjs/1.3.8/angular.min.js')}}
    {{HTML::script('js/libs/angular-animate.min.js')}}
    {{HTML::script('js/libs/toaster.js')}}
     {{HTML::script('js/libs/ngDraggable.js')}}
    {{HTML::script('js/custom/Factorys.js')}}
    {{HTML::script('js/custom/ToastCtrl.js')}}
    {{HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js')}}
    {{HTML::script('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js')}}
    @yield('scripts')
</head>
<body class="sinborde fondo">
    
    <div ng-controller="ToastCtrl">
  <toaster-container toaster-options="{'time-out': 3000}"></toaster-container>
  @yield('toasts')
  </div>
  
<ul class="nav nav-pills pull-left">
    @yield('logo')
</ul>
<div class="fondoHead nav nav-pills">
    <li>
        <h3 class="text-uppercase text-left textTitulo">ELEMENTS</h3>
    <li><h5 class="textSubTitulo">play with friends, for free now </h5></li>
    </li>
    @yield('navbar')
</div>

@yield('info')

<div>
    @yield('op')
</div>


<div class="container-fluid"></div>
    @yield('content')
</div>
<br><br>
<!--<footer>
    <div class="panel-footer footer">
        <p>&copy; Copyright 2014 Hospital de Trauma</p>
    </div>
</footer>-->
</body>
</html>