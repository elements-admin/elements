<!doctype html>
<html ng-app="elements">

<head>
	<meta charset="utf-8">
	<title>Elements</title>

	{{HTML::style('css/bootstrap.min.css')}} 
	{{HTML::style('css/jumbotron-narrow.css')}} 
	{{HTML::style('css/toaster.css')}} 
	{{HTML::script('//ajax.googleapis.com/ajax/libs/angularjs/1.3.8/angular.min.js')}} 
	{{HTML::script('js/libs/angular-animate.min.js')}}
	{{HTML::script('js/libs/toaster.js')}} 
	{{HTML::script('js/libs/ngDraggable.js')}} 
	{{HTML::script('https://cdn.socket.io/socket.io-1.2.0.js')}} 
	{{HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js')}} 
	{{HTML::script('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js')}}
	{{HTML::script('js/custom/Factorys.js')}} 
	{{HTML::script('js/custom/ToastCtrl.js')}}
	{{HTML::script('js/custom/SesionCrtl.js')}}
	@yield('scripts')

</head>

<body class="sinborde fondoLogged" style="overflow-y:hidden">

	<div ng-controller="ToastCtrl">
		<toaster-container ng-click="clickToast()" toaster-options="{'time-out': 3000}"></toaster-container>
		@yield('toasts')
		<div class="fondoHead nav nav-pills" style="height: 175px;">
			<div>
				<div class="textUser">
					@yield('username')
				</div>

				<img ng-src="@{{logo}}" class="round-img" data-toggle="modal" data-target="#miperfil" />{{Form::open(array('url' => 'modificarUsuario'))}}
				<br>
				<div class="modal fade" id="miperfil" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog  fondo-seleccionar-elemento" style="border: 1px solid white;">
						<div class="modal-content  fondo-seleccionar-elemento">
							<div class="modal-header color-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h3 style="text-align:center;"> Modificar Perfil de {{Auth::user()->user}}</h3>
							</div>

							<div class="modal-body" style="border: 1px solid white;">
								<div ng-repeat="x in datos">
									<div class="row ">
										<div class="col-xs-12" style="text-align:center;">
											<h4 class="">Nombre</h4>
											<input name="nombre" type="text" class=" input-centrado btn-lg" ng-minlength="4" ng-maxlength="10" value=@{{x.nombre}}>
											<input type="hidden" name={{Auth::user()->user}}/>
										</div>
										<div class="row ">
											<div class="col-xs-12" style="text-align:center;">
												<h4 class="">Correo</h4>
												<input name="correo" type="text" class="input-centrado btn-lg" value=@{{x.correo}}>
											</div>
										</div>
										<div class="row ">
											<div class="col-xs-12" style="text-align:center;">
												<h4 class="">Nueva Contraseña</h4>
												<input name="password" type="password" class=" input-centrado btn-lg">

											</div>
										</div>

									</div>
								</div>
							</div>
						</div>

						<div class="modal-footer">
							<div class="row">
								<div class="col-xs-6">
									<button type="submit" class="btn btn-primary btn-success btn-lg btn-block">Modificar</button>
								</div>
								<div class="col-xs-6">
									<button class="btn btn-primary btn-danger btn-lg btn-block" data-dismiss="modal">Cancelar</button>
								</div>
							</div>
						</div>


						</form>
						<br>
					</div>
				</div>
			</div>
			{{Form::close()}}
		</div>
	</div>
	<!-- <form action="game"> -->
	<input class="play boton" value="Play" type="submit" data-toggle="modal" data-target="#conectados">
	<!-- </form> -->

	<div ng-controller="SesionCtrl" class="modal fade" id="conectados" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header color-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 class="text-center">Usuarios conectados @{{nro_conectados}}</h3>
				</div>

				<div class="modal-body">
					<div class="chatContenedor">
						<!-- AQUI SE LISTARAN LOS USUARIOS CONECTADOS -->
						<ul class="list-group">
							<li class="list-group-item" ng-repeat="user in conectados track by $index">
								<div ng-if="noSoyYo(user)">
								<div class="col-lg-1">
									@{{user}}
								</div>
								<div class="col-lg-9">
								</div>
								
									<button ng-click="invitarUser(user)" class="btn btn-primary" style="  background: rgb(22,196,206);">
										Invitar
									</button>
								</div>
							
							</li>
						</ul>
					</div>
				</div>
				<div class="modal-footer">
					<div class="row">

						<div class="col-xs-2">
							
								<button  ng-click="invitarTodos()" class="btn btn-primary" style="  background: rgb(13,0,26);">Enviar Solicitud Abierta</button>
						
						</div>

					</div>
				</div>

			</div>
		</div>
	</div>






	<div class="aLaDer">
		<img src="img/logo.png" class="logoLogged">
		<p class="textTituloLogged">ELEMENTS</p>
		<p class="textSubTituloLogged">play with friends, for free now</p>
	</div>
	@yield('navbar')
	</div>


	<ul class="nav nav-pills pull-left">
		@yield('logo')
	</ul>

	@yield('info')

	<div style="width: 100%; padding: 0px;">
		@yield('content')
	</div>
	<br>
	<br>
	<!--<footer>
    <div class="panel-footer footer">
        <p>&copy; Copyright 2014 Hospital de Trauma</p>
    </div>
</footer>-->
</body>

</html>