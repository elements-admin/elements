@extends('layouts.master')
@section('navbar')
<ul class="nav-my">
    <li class="fondoCWhite">{{HTML::link("nuevoUsuario",'Registrarse')}}</li>
</ul>
@section('scripts')

{{HTML::script('js/custom/RegistroCtrl.js')}}
{{HTML::style('css/basics.css')}}
{{HTML::script('https://mandrillapp.com/api/docs/js/mandrill.js')}}
{{HTML::script('js/libs/send-email.js')}}

<script type="text/javascript">
//        sendTheMail('jhonnyx2012@gmail.com');
</script>
@stop
@section('logo')
<li><img src="img/logo.png"></li>
@stop

@section('toasts')
    @if (Session::has('error_login'))
        @{{pop('error','Usuario o contraseña incorrectos')}}
    @endif
    @if (Session::has('mensaje'))
         @{{pop('warning','Debes estar logueado para acceder a paginas del Juego')}}
    @endif
    @if (Session::has('cerrada'))
             @{{pop('success','Sesion cerrada correctamente')}}
    @endif
    @if (Session::has('creado'))
             @{{pop('success','Usuario creado correctamente')}}
    @endif
    @if (Session::has('modificado'))
             @{{pop('success','Perfil modificado correctamente')}}
    @endif
@stop

@section('content')

<div class="text-center text-primary bgimg">
    {{Form::open(array('url' => 'login'))}}<br>
        <div class="card" id="log-form">
            {{Form::label('user', 'Nick')}}<br>
            <input type="text" name="user" style="color:black;" value=""><br><br><br>
            {{Form::label('password', 'Password')}}<br>
            <input type="password" name="password" style="color:black;" value=""><br><br>
            <a href="" style="color:white;" data-toggle="modal" data-target="#micontrasena" >¿Olvidaste la contraseña?</a>
        </div>
        
        <input type="submit" class="entrar boton" value="Entrar"><br>
        
    {{Form::close() }}<br>
</div>
  <div ng-controller="RegistroCtrl"> 
    <div   class="modal fade" id="micontrasena" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog fondo-seleccionar-elemento" style="border: 1px solid white;">
        <div class="modal-content fondo-seleccionar-elemento ">
          
          <div class="modal-header color-header" >
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 style="text-align:center; color:white;"> Solicitar la Clave de Usuario</h3>
          </div>

          <div class="modal-body" style="border: 1px solid white;">
            <div class="row">
                <div class="col-xs-12" style="text-align:center;">
                    <h4 class="">Nick del Juego</h4>
                    <input type="text" class="input-centrado btn-lg"  ng-model="nick">
                </div>
              </div>
          </div>

          <div class="modal-footer">
            <div class="row">
              <div class="col-xs-6">
               <button class="btn btn-primary btn-success btn-lg btn-block" data-dismiss="modal" ng-click="enviarCorreo()"> Enviar</button>
              </div>
              <div class="col-xs-6">
                <button class="btn btn-primary btn-warning btn-lg btn-block" data-dismiss="modal">Cancelar</button>
              </div>

            </div>
            
          </div>

        </div>
      </div>
    </div>
  </div>
@stop