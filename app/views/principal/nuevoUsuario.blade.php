@extends('layouts.master')
@section('navbar')
<ul class="nav-my">
    <li class="fondoCWhite">{{HTML::link("/",'Login')}}</li>
</ul>

@section('logo')
<li><img src="img/logo.png"></li>
@stop


@section('scripts')
{{HTML::script('js/custom/RegistroCtrl.js')}}
{{HTML::style('css/basics.css')}}
@stop


@section('toasts')
   @if ($errors->has())
         <script type="text/javascript">
         var errores=[];
         @foreach ($errors->all() as $error)
             errores.push("{{$error}}");
          @endforeach
        </script>
        @{{pop('error','getErrores')}}
@endif

@stop

@section('content')
<div class="text-center text-primary bgimg" ng-controller="RegistroCtrl">
@if (Session::has('user'))
{{"";$user=Session::get('user')}}
@else
{{"";$user=Usuario::getModelo()}}
@endif

<div ng-controller="RegistroCtrl">
{{Form::open(array('url' => 'nuevoUsuario'))}}<br>
        <div class="card" id="registro-form">
        {{Form::label('nick', 'Nick')}}<br>
         <input type="text" name="user" required = "required" maxlength="10" style="color:black;" value="{{$user['user']}}"><br><br>
        {{Form::label('password', 'Password')}}<br>
        <input type="password" name="password" required = "required" maxlength="20" style="color:black;" value="{{$user['password']}}"><br><br>
        {{Form::label('nombre', 'Nombre')}}<br>
        <input type="text" name="nombre" required = "required" maxlength="40" style="color:black;" value="{{$user['nombre']}}"><br><br>
        {{Form::label('correo', 'Correo')}}<br>
        <input type="email" name="correo" required = "required" maxlength="40" style="color:black;" value="{{$user['correo']}}"><br><br>
        {{Form::label('elemento', 'Elemento')}}<br>
        <button type="button"  class="boton" data-toggle="modal" data-target="#miventana" style="font-family: eras;">
          @{{element}}
        </button>
        <input name="id_elemento" id="id_elemento" type="text" style="width:0px; height:0px; position:absolute; left:-50%" ng-model="idElement">        
    <div class="modal fade" id="miventana" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" >
        <div class="modal-content fondo-seleccionar-elemento" >
          
          <div class="modal-header color-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 align="center"> Seleccione un Elemento</h3>
          </div>
          
          <div class="modal-body" >
            
              <div class="row">
                  
                     
                  @foreach($elements as $elemento)
                    <div class="col-xs-3">
                      <center><input type="radio" name="opcion" id="{{$elemento->id}}"  value="{{$elemento->nombre}}"> </center>
                      <center><h4>{{$elemento->nombre}}</h4></center>
                      <img src="http://www.innovandroid.com.ve/web{{$elemento->ruta_logo}}" alt="..." class="img-circle" height="100px" width="100px">
                      <hr>
                      <p align="center">{{$elemento->descripcion}}</p>
                    </div>
                   @endforeach
                   
                  </div>
               </div>

          <div class="modal-footer">
            <button type="button" class="btn" data-dismiss="modal" aria-hidden="true" ng-click="myFunction()" style="color:black;">Seleccionar</button>
          </div>
            
          </div>

        </div>
      </div>
    </div>
        
<!--        <select name="id_elemento" style="color:black;">
            @foreach($elements as $elemento)
                 <option 
                @if($user['id_elemento']==$elemento->id)
                    selected
                @endif
               value="{{$elemento->id}}">{{$elemento->nombre}}</option>
            @endforeach
        </select> -->

        </div>

         <input type="submit"class="registro boton" value="Registrar"><br>
        {{Form::close()}}<br>
</div>
</div>
   
@stop