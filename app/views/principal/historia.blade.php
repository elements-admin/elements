@extends('layouts/master_logged')

@section('scripts')
{{HTML::script('js/custom/MazoCtrl.js')}}
{{HTML::style('css/basics.css')}}
@stop

@section('username')
    {{Auth::user()->user}}
@stop
@section('navbar')
    <ul class="nav-my-logged">
        <li class="fondoCWhite" style="width:150px;">{{HTML::link("home",'Inicio')}}</li>
        <li class="fondoCWhite" style="width:150px;">{{HTML::link("mazo",'Mazo')}}</li>
        <li class="fondoCWhite" style="width:150px;">{{HTML::link("store",'Tienda')}}</li>
          <li class="fondoCWhite active" >{{HTML::link("historia",'Historia')}}</li>
        <li class="fondoCWhite" style="width:150px;">{{HTML::link("acercade",'Acerca de')}}</li>
        <li class="fondoCWhite" style="width:150px;">{{HTML::link("logOut",'Logout')}}</li>
    </ul>
@stop

@section('content')
<div class="transparente1" >
    <div  class="historia1 container"> <h2 class="text-center ">Historia</h2></div>
    <div class="historia container"  style="overflow-y: auto; flex: 1;  overflow: auto; 
    height: calc(120vh - 245px);
    -webkit-overflow-scrolling: touch;
    -ms-overflow-style: none;" >
       
        <p align="justify"><br/>
            El mundo de <b>“Elements”</b> es el nuevo planeta tierra, en el que las batallas y la 

            lucha por reinar sobre los demás están a la orden del día. <br/><br/>

            El origen de este nuevo mundo parte del momento en el que el planeta colapsó, 

            desatando un proceso de deshumanización que dividió a todos sus habitantes en 
    
            cuatro regiones basadas en los elementos: tierra, aire, agua y fuego. <br/><br/>

            Al mismo tiempo, los habitantes de cada territorio comenzaron a notar que de la 

            nada surgían huecos del tiempo que traían consigo criaturas de distintas épocas: 

            guerreros, brujas, dragones, hadas, elfos, robots, magos, ángeles, duendes, 

            fantasmas, animales monstruosos, y muchos más. <br/><br/>

            A partir de ese momento, los humanos debían sobrevivir ante el caos que 

            comenzó a reinar en todos los territorios del planeta, uniendo sus fuerzas con las 

            de las criaturas del elemento al que pertenecían, asimismo, defendiéndose de 

            otras que tuvieran la necesidad de no relacionarse o de acabar con ellos.<br/><br/>

            La guerra de los elementos no es para cobardes, si deseas sobrevivir y tener un 

            lugar en este nuevo mundo, debes ser valiente y luchar con el apoyo de los 

            monstruos que estén de tu lado para acabar con el caos, así podrás destruir los 

            huecos del tiempo y unir los cuatro elementos nuevamente, para construir un 

            planeta lleno de paz.
        </p>
        
        
    </div>
       
</div>
@stop