@extends('layouts/master_logged')

@section('scripts')
{{HTML::script('js/custom/HomeCtrl.js')}}
{{HTML::script('js/custom/ChatCtrl.js')}}
{{HTML::style('css/basics.css')}}
{{HTML::style('css/estilos.css')}}
@stop

@section('username')
    {{Auth::user()->user}}
@stop

@section('navbar')
    <ul class="nav-my-logged">
        <li class="fondoCWhite active" >{{HTML::link("home",'Inicio')}}</li>
        <li class="fondoCWhite" >{{HTML::link("mazo",'Mazo')}}</li>
        <li class="fondoCWhite" >{{HTML::link("store",'Tienda')}}</li>
        <li class="fondoCWhite" >{{HTML::link("historia",'Historia')}}</li>
        <li class="fondoCWhite" >{{HTML::link("acercade",'Acerca de')}}</li>
        <li class="fondoCWhite" >{{HTML::link("logOut",'Logout')}}</li>
    </ul>
@stop

@section('toasts')
    @if (Session::has('success'))
        @{{pop('success','Logueado Correctamente')}}
    @endif
    @if (Session::has('modificado'))
             @{{pop('success','Perfil modificado correctamente')}}
    @endif
    @if ($errors->has())
         <script type="text/javascript">
         var errores=[];
         @foreach ($errors->all() as $error)
             errores.push("{{$error}}");
          @endforeach
        </script>
        @{{pop('error','getErrores')}}
    @endif
@stop

@section('content')
<div ng-controller="HomeCtrl">
    <div class="row-fluid fondo-home" style="height: calc(100vh - 175px);">
      <div ng-repeat="x in datos"  class="col-xs-3 fondo-medio-transparente" style="overflow-y: auto; flex: 1;  overflow: auto; height: calc(100vh - 175px);">
      <div style="  display: flex; flex; padding-bottom:23%;
     align-content: center;  center;"></div>
     
        <div class="row-fluid">
        <center><h6>NOMBRE</h6></h6></center>
          <center><h4 ng-bind="x.nombre"></h4></center>
        </div>
        <div class="row-fluid">
          <center><h6>ELEMENTO</h6></center>
          <center><h4 ng-bind="x.elemento"></h4></center>
        </div>
   <!--       <div style="padding-top:15px"></div>  --> 
        <div class="row-fluid">
          <center><h6>NIVEL</h6></center>
          <center><h4 ng-bind="x.nivel"></h4></center>
        </div>
   <!--       <div style="padding-top:15px"></div>  --> 
        <div class="row-fluid ">
          <center><h6 class="color-naranja">ORO</h6></center>
          <center><h4 ng-bind="x.oro" class="color-naranja"></h4></center>
        </div>
   <!--       <div style="padding-top:15px"></div>  --> 
        <div class="row-fluid">
          <center><h6 class="color-verde">EXPERIENCIA</h6></center>
          <center><h4 ng-bind="x.experiencia" class="color-verde"></h4></center>
        </div>
 <!--       <div style="padding-top:15px"></div>  --> 
        <div class="row-fluid">
          <center><h6>BATALLAS PERDIDAS</h6></center>
          <center><h4 ng-bind="x.perdidas"></h4></center>
        </div>
<!--       <div style="padding-top:15px"></div>  --> 
        
        <div class="row-fluid">
          <center><h6>BATALLAS GANADAS</h6></center>
          <center><h4 ng-bind="x.ganadas"></h4></center>
        </div>
        
     <div class="row-fluid">
          <center><h6>TOTAL BATALLAS</h6></center>
          <center><h4 ng-bind="x.total_batallas"></h4></center>
        </div>
        
        <div class="row-fluid">
          <center><h6>CORREO</h6></center>
          <center><h4 ng-bind="x.correo"></h4></center>
        </div>
        
        <div style="padding-top:30px"></div>  
      </div>
      
      
      <div class="col-xs-1"></div>
      
      
      <!-- PARTE CENTRAL DEL CONTENIDO -->
      
  
      <div class="col-xs-4" style="overflow-y: auto; flex: 1;  overflow: auto; 
    height: calc(100vh - 175px);
    -webkit-overflow-scrolling: touch;
    -ms-overflow-style: none;">
        
        <div style="padding-top:25px" ng-repeat="a in act">
           <div class="row card">
          <center>@{{a.descripcion}}</center>
        </div>
        </div>
       
      </div> 



      <div class="col-xs-1"></div>
      <!-- PARTE DERECHA DEL CONTENIDO -->
  <div  ng-controller="ChatCtrl">
      
      <div  class="col-xs-3 fondo-medio-transparente-derecho" style="height: calc(100vh - 175px);">
      
        <div style="padding-bottom:20%;"></div>
        
    
        
        <center ng-repeat="mazo in m">
          
          <img class="mazo-home" style="bottom:@{{$index/3}}%; left:@{{$index/3}}%;" ng-src="@{{url}}@{{mazo.ruta}}">
        </center>
        
        <div style="padding-top:20px"></div>
        
        <div style="position:absolute; bottom:10px;">
           <button ng-click="reiniciar()" class="boton boton-chat" data-toggle="modal" data-target="#miventana">
			CHAT <span class="badge">@{{nro_mensajes}}</span>
			</button>
       </div>
        <div style="padding-top:15px"></div>  
        </div>
     
  <div class="modal fade" id="miventana" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog fondo-seleccionar-elemento">
        <div class="modal-content fondo-seleccionar-elemento">
          
          <div class="modal-header color-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 style="text-align:center"> Chat General de Elements</h3>
          </div>

          <div class="modal-body fondo-seleccionar-elemento" style="border: 1px solid white;">
            <div class="chatContenedor">
				<div class="row" ng-repeat="msg in messages track by $index">
						<div class="col-xs-12" style=" color:white; padding-top:10px ;  padding-bottom:10px ;  border-bottom:1px solid rgb(255,177,63);"><b>@{{msg.name}}:&nbsp;</b>@{{msg.message}}</div>
				</div>
            </div>
          </div>
          <div class="modal-footer" style="border: 1px solid white;">
            <div class="row">
              <div class="col-xs-10">
                <input type="text" class="form-control" placeholder="Escribe el Mensaje a Enviar" ng-enter="boton_chat()" ng-model="mensaje">
			 </div>
              <div class="col-xs-2">
				<button ng-click="boton_chat()" class="btn btn-primary" style="  background: rgb(16,13,35);">
					Enviar
				</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	</div>
</div>
@stop