@extends('layouts/master_logged')

@section('scripts')
{{HTML::script('js/custom/MazoCtrl.js')}}
{{HTML::style('css/basics.css')}}
@stop

@section('username')
    {{Auth::user()->user}}
@stop
@section('navbar')
    <ul class="nav-my-logged">
        <li class="fondoCWhite" style="width:150px;">{{HTML::link("home",'Inicio')}}</li>
        <li class="fondoCWhite active" style="width:150px;">{{HTML::link("mazo",'Mazo')}}</li>
        <li class="fondoCWhite" style="width:150px;">{{HTML::link("store",'Tienda')}}</li>
        <li class="fondoCWhite" >{{HTML::link("historia",'Historia')}}</li>
        <li class="fondoCWhite" style="width:150px;">{{HTML::link("acercade",'Acerca de')}}</li>
        <li class="fondoCWhite" style="width:150px;">{{HTML::link("logOut",'Logout')}}</li>
    </ul>
@stop

@section('content')
<div class="transparente" ng-controller="MazoCtrl">
    <div >
       <ul class="nav-my-tienda">
        
        <button class="fondoCWhite active" style="width:150px;" ng-click="cargarCartasMazo()"ng-show="selmazo">Mazo</button>
        <button class="fondoCWhite" style="width:150px;" ng-click="cargarCartasAlmacen()"ng-show="selmazo">Almacen</button>
        <button class="fondoCWhite " style="width:150px;" ng-click="cargarCartasMazo()"ng-show="selalmacen">Mazo</button>
        <button class="fondoCWhite active" style="width:150px;" ng-click="cargarCartasAlmacen()"ng-show="selalmacen">Almacen</button>
        
        
       </ul>
       <button class="boton btnmazo" style="width:150px;" ng-click="enviaralm(seleccionadas)" ng-hide="visible2">Mandar Almacen</button>
        <button class="boton btnmazo" style="width:150px;" ng-show="visible1"ng-click="enviarmaz(seleccionadas)">Mandar al Mazo</button> 
    </div>
    
      <div class="tabla-cartas" style="padding-top:80px;"></div> 

            <ul class="g tabla-cartas" style="overflow-y: auto; flex: 1;  overflow: auto; 
    height: calc(115vh - 225px);
    -webkit-overflow-scrolling: touch;
    -ms-overflow-style: none;">
            <li  ng-repeat="x in cartas" id="@{{x.nombre}}">
                <img ng-click="select(x.id,$event)" class="td-mazo" ng-src="@{{url}}@{{x.ruta}}" width="250">
            </li>
            </ul>
</div>
@stop