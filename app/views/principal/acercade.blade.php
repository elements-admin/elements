@extends('layouts/master_logged')

@section('scripts')
{{HTML::style('css/basics.css')}}
@stop

@section('username')
    {{Auth::user()->user}}
@stop
@section('navbar')
    <ul class="nav-my-logged">
        <li class="fondoCWhite" style="width:150px;">{{HTML::link("home",'Inicio')}}</li>
        <li class="fondoCWhite" style="width:150px;">{{HTML::link("mazo",'Mazo')}}</li>
        <li class="fondoCWhite" style="width:150px;">{{HTML::link("store",'Tienda')}}</li>
           <li class="fondoCWhite" >{{HTML::link("historia",'Historia')}}</li>
        <li class="fondoCWhite active" style="width:150px;">{{HTML::link("acercade",'Acerca de')}}</li>
        <li class="fondoCWhite" style="width:150px;">{{HTML::link("logOut",'Logout')}}</li>
    </ul>
@stop
@section('content')
<div class="transparente1">
<div class="historia container"style="overflow-y: auto; flex: 1;  overflow: auto; 
    height: calc(120vh - 180px);
    -webkit-overflow-scrolling: touch;
    -ms-overflow-style: none;" >
    
    <div class="cardacerca" style="padding-top:80px ">
      <span>
        Elements es un Juego de cartas que nace bajo un proyecto de la asignatura Administracion de la Informacion II<br/>
        <br/>
        <div class="letraacerca">CREDITOS</div>
        <br/>
        <div class="letraacerca">Equipo de Programación</div><br/>
        
        Jhonny Barrios<br/>
        Hector Morales<br/>
        Kenny Perroni<br/>
        Rendall Rojas<br/>
        Victor Roman<br/>
        Eduardo Yzcaray<br/>
        
        <br/>
        
        <div class="letraacerca">Equipo de RRHH</div><br/>
        
        Tibisay Marsden<br/>
        Ornela Pisellini<br/>
        Maria Tieno<br/>
        
        <br/>
        <div class="letraacerca">Equipo de Base de Datos</div><br/>
        
        Antonio Aponte <br/>
        Manuel Carvajal <br/>
        Adrian Peñaloza <br/>
        
        <br/>
        <div class="letraacerca">Equipo de Diseño</div><br/>
        
        Jesús Ameneiro <br/>
        Oscar Bolívar<br/>
        Jhon Castro <br/>
        Kevin Flores <br/>
        Rafael Mayo<br/>
        
        
      </span>
    </div>
    
</div>
</div>
@stop