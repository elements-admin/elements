@extends('layouts/master_logged')

@section('scripts')
{{HTML::script('js/custom/TiendaCtrl.js')}}
{{HTML::style('css/basics.css')}}
@stop

@section('username')
    {{Auth::user()->user}}
@stop
@section('navbar')
    <ul class="nav-my-logged">
        <li class="fondoCWhite" style="width:150px;">{{HTML::link("home",'Inicio')}}</li>
        <li class="fondoCWhite" style="width:150px;">{{HTML::link("mazo",'Mazo')}}</li>
        <li class="fondoCWhite active" style="width:150px;">{{HTML::link("store",'Tienda')}}</li>
        <li class="fondoCWhite" >{{HTML::link("historia",'Historia')}}</li>
        <li class="fondoCWhite" style="width:150px;">{{HTML::link("acercade",'Acerca de')}}</li>
        <li class="fondoCWhite" style="width:150px;">{{HTML::link("logOut",'Logout')}}</li>
    
    </ul>
@stop

@section('toasts')
  @if (Session::has('creado'))
             @{{pop('success','Usuario creado correctamente')}}
    @endif
    
@stop

@section('content')

<div ng-controller="TiendaCtrl" >
    <div class="transparente" ng-repeat="x in datos">

       <ul class="nav-my-tienda ">
       
        <button class="fondoCWhite" style="width:150px;" ng-click="cargarmagicas()"ng-show="cargartodas">Magicas</button>
        <button class="fondoCWhite" style="width:150px;" ng-click="cargarm()" ng-show="cargartodas">Monstruos</button>
        
        <button class="fondoCWhite active" style="width:150px;" ng-click="cargarmagicas()"ng-show="cargarmag">Magicas</button>
        <button class="fondoCWhite" style="width:150px;" ng-click="cargarm()" ng-show="cargarmag">Monstruos</button>
        
        <button class="fondoCWhite " style="width:150px;" ng-click="cargarmagicas()"ng-show="cargarmonstruos">Magicas</button>
        <button class="fondoCWhite active" style="width:150px;" ng-click="cargarm()" ng-show="cargarmonstruos">Monstruos</button>
        
        
       </ul>
        <div class=" oro ">
          <center><h6 class="color-naranja">ORO</h6></center>
          <center><h4 ng-bind="x.oro" class="color-naranja"></h4></center>
        </div>
       </div>
        

      
            <button name="comprar" class="comprar boton"ng-click="comprarCartas(seleccionadas)">
               Comprar
            </button>
          <div class="tabla-cartas" style="padding-top:15px;"></div> 

            <ul class="g tabla-cartas" style="overflow-y: auto; flex: 1;  overflow: auto; 
    height: calc(95vh - 225px);
    -webkit-overflow-scrolling: touch;
    -ms-overflow-style: none;">
            <li  ng-repeat="x in cartas" id="@{{x.nombre}}">
                <img ng-click="select(x.id,$event)" class="td-mazo" ng-src="@{{url}}@{{x.ruta}}" width="250"><div class="precioletra" align="center"><b>Precio: @{{x.precio}} MdO</b></div>
            </li>
            </ul>
        
       
             
</div>
           
      
@stop