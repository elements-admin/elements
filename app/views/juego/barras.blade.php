
        <!-- Aqui va la parte del progress bar del 1-->
       
       
       <div >
            <div ng-if="tuturno">
			            <img id="logo" class="bordeTurno" ng-src="@{{logo}}" width="70px" height="70px" style="position:absolute; top:10px; left:-20%; display:block"></img>

			</div>
			<div ng-if="!tuturno">
			            <img id="logo" class="bordeNoTurno" ng-src="@{{logo}}" width="70px" height="70px" style="position:absolute; top:10px; left:-20%; display:block"></img>

			</div>   
          </div>
       
       <div >
                <div ng-if="tuturno">
					<img ng-src="@{{logoOp}}" class="bordeNoTurno" width="70px" height="70px" style="position:absolute; top:10px; right:-20%; display:block"></img>
			</div>
			<div ng-if="!tuturno">
					<img ng-src="@{{logoOp}}" class="bordeTurno" width="70px" height="70px" style="position:absolute; top:10px; right:-20%; display:block"></img>

			</div>
            
          </div>
       
       
       
        <div class="row" style="background-color: rgba(13,0,26,0.8); border-radius: 10px; padding-top:7px;">
        
          
          
          <div class="col-xs-2" >
            <p>{{Auth::user()->user;}}</p> <!-- Nombre de jugador en barra superior-->
          
          </div>
          <div class="col-xs-3">
            
            <p>Puntaje: @{{punto}}</p>
            
            </div>
          
          <div class="col-xs-2 "></div>
          
          <div class="col-xs-2 " >
            <p>@{{datosOponente.user}}</p>
          </div>
          <div class="col-xs-3 " >
           <p>Puntaje: @{{puntosOp}}</p>
          </div>
        </div>

        <!-- Aqui va la parte del progress bar del 2-->
        <div class="row" style="padding-top:10px; padding-bottom:0px;">

          <div class="progress col-xs-5" style="padding:0px">
            <!--barra carta jugador 1-->
            <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" style="width:@{{barra}}%"></div>
          </div>


          <div class="col-xs-2">
            <!--Separador de Barra-->
          </div>

          <div class="progress col-xs-5 rotar" style="padding:0px">
            <!--barra carta jugador 2-->
            <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" style="width:@{{barraOp}}%"></div>
          </div>

        </div>
        
     