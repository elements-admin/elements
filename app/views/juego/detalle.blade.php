   <div class="col-xs-1">
        <div style="border: 1px solid white; position:absolute; background-color: rgba(13,0,26,0.6);">
          <center>
            <h4 style="color:#FFFFFF;">Detalle de carta</h4>
          </center>
          <img ng-src="@{{detalle}}" width="290px" height="400"></img>
	   <center ng-if="tieneAtaque()">
			  <h4 style="color:#FFFFFF;">Ataque Restante: @{{ataque_restante}}</h4>
          </center>
        </div>
        <div class="row" style="width:290px; position:absolute;"> <center></center></div>
       <div id="caja" class="row" style="border: 1px solid white; background-color: rgba(13,0,26,0.6);">
      
      <div class="row">
        <div class="row" style="">
			<div ng-if="tuturno">
			<center><h4 class="text-success">ES TU TURNO</h4></center>
			</div>
			<div ng-if="!tuturno">
			<center><h4 class="text-danger">NO ES TU TURNO</h4></center>
			</div>
        </div>
         <div class="col-xs-6">
           <button type="button" ng-click="pasar_button();" class="btn btn-lg btn-block" style="background-color:rgb(0,51,0);">Fin de Turno</button>
         </div>
         <div class="col-xs-6">
           <button type="button" class="btn btn-lg btn-block" style="display:none" data-toggle="modal" data-target="#miventanaperdedor" style="background-color:rgb(51,0,0);">Rendirse</button>
           <button class="btn btn-lg btn-block" data-toggle="modal" data-target="#miventanaganador" style="display:none; background-color:rgb(51,0,0);">Ganar</button>
         
		  </div>
       </div>
      <div style="padding-top:5px"></div>
   <div ng-controller="ChatCtrl" class="row">
         <div class="col-xs-12">
           <button  ng-click="reiniciar()" class="btn  btn-lg btn-block" data-toggle="modal" data-target="#miventana" style="background-color:rgb(13,0,26);">
              Chat con @{{datosOponente.user}}
			    <span class="badge">@{{nro_mensajes}}</span>
           </button>
         </div>
       
       
          <div  class="modal fade" id="miventana" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog fondo-seleccionar-elemento">
                <div class="modal-content fondo-seleccionar-elemento">
              
                  <div class="modal-header color-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <center>
                            <h3> Chat con @{{datosOponente.user}}</h3>
                        </center>
                  </div>
    
              <div class="modal-body" style="border:1px solid white;">
                <div class="chatContenedor">
                
					<div class="row" ng-repeat="msg in messages_ind track by $index">
						<div class="col-xs-12" style=" color:white; padding-top:10px ;  padding-bottom:10px ;  border-bottom:1px solid rgb(255,177,63);"><b>@{{msg.name}}:&nbsp;</b>@{{msg.message}}</div>
					</div>
				
                </div>
              </div>
    
              <div class="modal-footer" style="border:1px solid white;">
                <div class="row">
                  <div class="col-xs-10">
                    <input type="text" class="form-control" placeholder="Escribe el Mensaje a Enviar" ng-model="mensaje" ng-enter="boton_enviar()" >
                  </div>
                  <div class="col-xs-2">
                    <button class="btn btn-primary" ng-click="boton_enviar()" >Enviar</button>
                  </div>
    
                </div>
                
              </div>
    
            </div>
          </div>
        </div>
       		</div>
		   
		   <div class="modal fade" id="miventanaperdedor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog fondo-seleccionar-elemento">
                <div class="modal-content fondo-seleccionar-elemento">
              
                  <div class="modal-header color-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <center>
                            <h3> Has  perdido contra @{{datosOponente.user}}</h3>
							<h4>Experiencia ganada: @{{exp}}</h4>
							<h4>Oro ganado: @{{oro}}</h4>
                        </center>
                  </div>
    
              <div class="modal-body" style="border:1px solid white;">
                <div class="row">
					<div class="col-xs-12" style=" color:white; padding-top:10px;  padding-bottom:10px;">
						<h3>Has Perdido!!  </h3>
					</div>
				</div>
			  </div>
              <div class="modal-footer" style="border:1px solid white;">
                  <button class="btn btn-primary btn-danger" ng-click="home()">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
       
		   <!--MODAL GANADOR -->
		   
		   <div class="modal fade" id="miventanaganador" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog fondo-seleccionar-elemento">
                <div class="modal-content fondo-seleccionar-elemento">
              
                  <div class="modal-header color-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <center>
                            <h3> Has ganado a @{{datosOponente.user}}</h3>
							<h4>Experiencia ganada: @{{exp}}</h4>
							<h4>Oro ganado: @{{oro}}</h4>
															
                        </center>
                  </div>
    
              <div class="modal-body" style="border:1px solid white;">
                <div class="row">
					<div class="col-xs-12" style=" color:white; padding-top:10px;  padding-bottom:10px;">
						<h3>Has Ganado!!</h3>
					</div>
				</div>
			  </div>
    
              <div class="modal-footer" style="border:1px solid white;">
                
                  <button class="btn btn-primary btn-danger"  ng-click="home()">Cerrar</button>
                
              </div>
    
            </div>
          </div>
        </div>
       
      </div>
    </div>
      </div>