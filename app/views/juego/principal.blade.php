<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Elements</title>
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.3/jquery-ui.js"></script>
	 <script type="text/javascript">
         var datosOponente={{$_POST["datosOponente"]}};
        </script>
	
	{{HTML::style('css/estilos.css')}}
     <!--<link rel="stylesheet" href="" media="screen">-->
    {{HTML::style('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css')}}
    {{HTML::style('css/toaster.css')}}
	{{HTML::style('css/jquery-ui.css')}}
	{{HTML::script('js/libs/jquery-ui.js')}}
    {{HTML::script('//ajax.googleapis.com/ajax/libs/angularjs/1.3.8/angular.min.js')}}
	{{HTML::script('https://cdn.socket.io/socket.io-1.2.0.js')}} 
    {{HTML::script('js/libs/angular-animate.min.js')}}
    {{HTML::script('js/libs/toaster.js')}}
    {{HTML::script('js/libs/ngDraggable.js')}}
    {{HTML::script('js/custom/Factorys.js')}}
    {{HTML::script('js/custom/ToastCtrl.js')}}
    {{HTML::script('js/custom/GameCtrl.js')}}
	{{HTML::script('js/custom/ChatCtrl.js')}}
   
    {{HTML::script('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js')}}
	 
</head>
<body ng-app="elements"  style="border:0px padding:0px" ng-controller="GameCtrl">
	<div >
		
  <div ng-controller="ToastCtrl">
    <toaster-container toaster-options="{'time-out': 3000}"></toaster-container>
       <!-- @{{pop('warnig','EN CONSTRUCCION')}}-->
</div>
  <div style="padding-top:10px"></div>
  <div class="container" style="max-width: none !important; width: 970px;">
    <div class="row">
      <div class="col-xs-8 ">
        @include('juego/barras')
        @include('juego/tablero')
      </div>
      <div class="col-xs-1"></div>
      <div class="col-xs-1"></div>
      @include('juego/detalle')
    </div>
	  </div>
</body>
</html>