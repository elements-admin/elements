
         <!-- aqui va a mostrar las cartas en mano  del usuario oponente -->
        
        <toaster-container toaster-options="{'time-out': 3000}"></toaster-container>
		@yield('toasts')
        <div class="row revez">
          <div class="col-xs-1" style="padding-left:  2px; padding-right: 2px;transform: translate(345%, 50%);" ng-repeat="img in enManoOp track by $index">
           	<div ng-if="!isTransparent(img)">
				<img   class="imgcenter" ng-src="@{{urlB}}" width="50px" height="85px">
			  </div>
			  	<div ng-if="isTransparent(img)">
				<img   class="imgcenter" ng-src="@{{urlT}}" width="50px" height="85px">
			  </div>
			  
          </div>
        </div>

        <div class="fondo p">
          <!-- aqui va a mostrar las cartas magicas del usuario oponente -->
          <div class="row rotar">
            <div class="col-xs-4 "></div>
            <div class="col-xs-2 fondo" ng-repeat="img in mgOp">
              <img ng-mouseover="showDetalle(img)" class="imgcenter" ng-src="@{{url}}@{{img.ruta}}" width="70px" height="95px">
            </div>
            <div class="col-xs-1 "></div>
			  <!-- aqui se va a mostrar el mazo del oponente -->
            <div class="col-xs-3" >
				<div ng-repeat="img in mazoOp">
					<center><img  class="mazo" style="bottom:@{{$index/50}}%; left:@{{(-38+$index/3)}}%;" ng-src="@{{urlB}}" width="70px" height="95px">
            		</center>
				</div>
			</div>
          </div>

          <!-- aqui va a mostrar las cartas de monstruos del usuario oponente -->
          <div class="row rotar">
            <div class="col-xs-3"></div>
            <div class="col-xs-2 fondo" ng-repeat="img in mOp" id="@{{img.id}}">
              <img ng-mouseover="showDetalle(img)" ng-click="recibe_ataque($index)" class="imgcenter" ng-src="@{{url}}@{{img.ruta}}" width="70px" height="95px" />
            </div>
           <!-- aqui va a mostrar el cementerio del oponente -->
			 <div class="col-xs-3" >
             	<div ng-repeat="img in cOp">
				 	<img ng-mouseover="showDetalle(img)" class="mazo" style="bottom:@{{$index/50}}%; left:@{{-30+($index/3)}}%;" ng-src="@{{url}}@{{img.ruta}}" width="70px" height="95px" />
            	</div>
			 </div>
          </div>

          <!-- aqui va a mostrar las cartas de monstruos de tu mazo -->
          <div class="row">
            <div class="col-xs-3"></div>
            <div class="col-xs-2 fondo noMazo" ng-drop="true" ng-drop-data="img" ng-drop-success="onDropCompleteM($index,$data,$event)" ng-repeat="img in mTu" id="@{{img.id}}">
              <img ng-mouseover="showDetalle(img)" ng-click="atacar($index,$data,$event)" class="imgcenter" ng-src="@{{url}}@{{img.ruta}}" width="70px" height="95px"/>
            </div>
             <!-- aqui va a mostrar el cementerio tuyo -->
			  <div class="col-xs-3" ng-repeat="img in cTu">
              <img class="mazo" ng-mouseover="showDetalle(img)"  style="bottom:@{{$index/50}}%; left:@{{$index/3}}%;" ng-src="@{{url}}@{{img.ruta}}" width="70px" height="95px" />
            </div>
          </div>
 
          <!-- aqui va a mostrar las cartas magicas de tu mazo -->
          <div class="row">
            <div class="col-xs-4"></div>
            <div class="col-xs-2 fondo noMazo" ng-drop="true" ng-drop-data="img" ng-drop-success="onDropCompleteMg($index,$data,$evt)" ng-repeat="img in mgTu">
              <img ng-mouseover="showDetalle(img)" class="imgcenter" ng-src="@{{url}}@{{img.ruta}}" width="70px" height="95px" />
            </div>
            <div class="col-xs-1 fondo"></div>
            
            <div class="numero_tumazo">@{{mazo.length}}</div>
             <div class="col-xs-3" ng-repeat="carta in mazo">
             <img class="mazo"   style="bottom:@{{$index/50}}%; left:@{{$index/3}}%;" ng-drag="true" ng-drag-data="carta" ng-src="@{{urlB}}"  width="70px" height="95px"/>
            </div>
            
          </div>
        </div>
        <!-- separador  -->
        <div style="padding-top:0px"></div>

        <!-- aqui va a mostrar las cartas de la mano  -->
        <div class="row q">
          <div class="col-xs-1"></div>
          <div class="col-xs-2" ng-drop="true" ng-drop-data="carta" ng-drop-success="onDropCompleteMazo($index,$data,$evt)" ng-repeat="img in enMano" style="padding-left:  2px; padding-right: 2px;">

            <img class="master" ng-mouseover="showDetalle(img)" ng-drag="true" ng-drag-data="img" ng-drag-success="onDragSuccess($data)" class="imgcenter" ng-src="@{{url}}@{{img.ruta}}" width="90px" height="140px" />
          </div>
          <div class="col-xs-1"></div>
        </div>