<?php 
class UsuariosController extends Controller {

    /**
     * Crear el usuario nuevo
     */
     
     protected $correoR;
     protected $passwordR;
     
     public function getCorreo($nick)
     {
		   $usuario =  DB::table('usuario')->where('user', $nick)->first();
		   if (is_null ( $usuario ))
            return "";
		 
       return $usuario->correo;
     }
     
     public function getClave($nick)
     {
       $id =  DB::table('usuario')->where('user', $nick)->first();
       
       if (is_null ( $id ))
            return "";
       
       
       $usuario= Usuario::find($id->id);
       
       
            
       $clave_provicional=str_random(10);
       $usuario->password=Hash::make($clave_provicional);
       $usuario->save();
       return $clave_provicional;
     }
     
    public function crearUsuario()
    {
        // Creamos un nuevo objeto para nuestro nuevo usuario
        $usuario = new Usuario;
        // Obtenemos la data enviada por el usuario
        $data = Input::all();
        // Revisamos si la data es válido
         //$usuario = e(Input::get('usuario'));
         //$password = e(Input::get('password'));
        // $email = e(Input::get('email'));
        // $nombre = e(Input::get('nombre'));
        
          
        if ($usuario->isValid($data))
        {
            $data=array(
                'user'=>e(Input::get('user')),
                'nombre'=>e(Input::get('nombre')),
                'password'=>Hash::make(Input::get('password')),
                'correo'=>e(Input::get('correo')),
                'id_elemento'=>e(Input::get('id_elemento'))
            );
            // Si la data es valida se la asignamos al usuario
            $usuario->fill($data);

            // Guardamos el usuario
            $usuario->save();
            Session::put('current_user', Input::get('user'));
            // Y Devolvemos una redirección a la acción show para mostrar el usuario
            return Redirect::to('/')->with('creado',true);
        }
        else
        {
            // En caso de error regresa a la acción create con los datos y los errores encontrados
            return Redirect::to('nuevoUsuario')->withErrors($usuario->errors)->with('user', Input::all())->withInput();
        }
    }
    
        public function modificarUsuario()
    {
        $id = Auth::user()->id;
        $usuario= Usuario::find($id);
       
        
        
        // Creamos un nuevo objeto para nuestro nuevo usuario
        
        // Obtenemos la data enviada por el usuario
         $data = Input::all();
        // Revisamos si la data es válido
         //$usuario = e(Input::get('usuario'));
         //$password = e(Input::get('password'));
        // $email = e(Input::get('email'));
        // $nombre = e(Input::get('nombre'));
        $data=array(
                
                'nombre'=> e(Input::get('nombre')),
                'correo'=> e(Input::get('correo')),
                'password'=>e(Input::get('password')),
                
                //'id_elemento'=>e(Input::get('id_elemento'))
            );
            
        //if ($usuario->correo==$data[0].correo)
        if (strcmp ($usuario->correo , $data['correo'] ) == 0)
            {
                
                if (strcmp ( "" , $data['password']  ) == 0)
                {
                  $data2=array(
                    'nombre'=>e(Input::get('nombre')),
                    );
                  
                  $usuario->fill($data2);
                  $usuario->save();
                }
                else
                {
                  $data2=array(
                    'nombre'=>e(Input::get('nombre')),
                    'password'=>Hash::make(Input::get('password')),
                  );   
                
                  $usuario->fill($data2);
                  $usuario->password=Hash::make(Input::get('password'));
                  $usuario->save();
                }
                
                  
                return Redirect::to('home')->with('modificado',true);
            }
        else
        {
            // si la clave esta vacia entonces revisamos el correo
            if (strcmp ( "" , $data['password']  ) == 0)
                if ($usuario->validarCorreo(array( 'correo'=>e(Input::get('correo')))))
                {
                    $data2=array(
                    'nombre'=>e(Input::get('nombre')),
                    'correo'=>e(Input::get('correo')),
                    );         
                    
                     $usuario->fill($data2);
                     $usuario->save();
                     return Redirect::to('home')->with('modificado',true);
                    
                }
            
            if ($usuario->isValidModificar($data))
            {
                // Si la data es valida se la asignamos al usuario
                $usuario->fill($data);
                $usuario->password=Hash::make(Input::get('password'));
                // Guardamos el usuario
                $usuario->save();
                //Session::put('current_user', Input::get('user'));
                // Y Devolvemos una redirección a la acción show para mostrar el usuario
                return Redirect::to('home')->with('modificado',true);
            }
        else
        // En caso de error regresa a la acción create con los datos y los errores encontrados
             return Redirect::to('home')->withErrors($usuario->errors)->with('user', Input::all())->withInput();
        
            
        }
    } 
   protected function getInfoUser()
   {
        $id = Auth::user()->id;
  
        return DB::select(DB::raw('call getInfo('.$id.')'));
   }
	
	protected function actualizarPerfil($bandera){
		
				
		$id = Auth::user()->id;
		$act = Usuario::find($id);
		$oro = 500;
		$exp = 25;
		$aux_exp = 0;
		$nivel = 0;
		
		//Actualizar ganador
		if($bandera == 1){
			
		$oro += $act->oro;
		$exp = $exp/$act->nivel;
		$exp += $act->experiencia;	
			
	   DB::table('usuario')
       ->where('id', $id)
       ->update(array(
         'oro' => $oro,
         'experiencia' => $exp      
  ));
			
			if($exp >= 100){
			 
			 $nivel = $act->nivel+1;
			 $aux_exp = $exp - 100;
			DB::table('usuario')
            ->where('id', $id)
            ->update(array(
              'nivel' => $nivel, 
			  'experiencia' => $aux_exp	
				
  ));
			
			}
			
		

			
		}
		
		//Actualizar perdedor
		else{
		
		$oro = $oro/3;	
		$oro += $act->oro;
		$exp = ($exp/2)/$act->nivel;			
		$exp += $act->experiencia;	
			
	   DB::table('usuario')
       ->where('id', $id)
       ->update(array(
         'oro' => $oro,
         'experiencia' => $exp      
  ));
			
			if($exp >= 100){
			 
			 $nivel = $act->nivel+1;
			 $aux_exp = $exp - 100;
			DB::table('usuario')
            ->where('id', $id)
            ->update(array(
              'nivel' => $nivel, 
			  'experiencia' => $aux_exp	
  ));
			
			
		}
			
		}
		
	}
   

   
   
   
   
        /**
     * Store a newly created resource in storage.
     *
     * @return Response

    public function store()
    {
    Comment::create(array(
    'author' => Input::get('author'),
    'text' => Input::get('text')
    ));

    return Response::json(array('success' => true));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response

    public function destroy($id)
    {
    Comment::destroy($id);

    return Response::json(array('success' => true));
    }*/
}
?>