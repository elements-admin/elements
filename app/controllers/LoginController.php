<?php
class LoginController extends Controller
{
    //establecemos restful a true
    public $restful = true;

    //anteponemos post al nombre de la función, esto es así porque es la función
    //que recibirá datos por post
    public function post_index()
    {
        //recogemos los campos del formulario y los guardamos en un array
        //para pasarselo al método Auth::attempt
     
        $userdata = array(
            'user' => Input::get('user'),
            'password'=> Input::get('password')
        );

        //si los datos son correctos y existe un usuario con los mismos se inicia sesión
        //y redirigimos a la home
        if(Auth::attempt($userdata, true))
        {
            Session::put('current_user', Input::get('user'));
            return Redirect::to('home')->with('success', true);
        }
        else
        {
            //en caso contrario mostramos un error
            return Redirect::to('/')->with('error_login', true);
        }
    }

    //con este sencillo metódo cerramos la sesión del usuario
    public function logOut()
    {
        Auth::logout();
        Session::flush();
        return Redirect::to('/')->with('cerrada','¡Has cerrado sesión correctamente!.');

    }
}