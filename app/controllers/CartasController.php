<?php

class CartasController extends Controller 
{
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	 
	protected function getAll()
	{
		 return Carta::all();
		 //	$id = Auth::user()->id;
		//  return Carta::where('elemento','=',1)->get();
		 //return Carta::where('id','<>','0')->orderByRaw("RAND()")->take(40)->get(); 
		 //return Carta::join('mazo','carta.id','=','mazo.id_carta')->where('mazo.id_jug','=',1)->where('mazo.estatus','=',1)->where('mazo.seleccion','=',0)->orderByRaw("RAND()")->get();
		//return Carta::leftJoin('mazo','carta.id','=','mazo.id_carta')->where('mazo.id_jug','=',57)->get();
		

//	return Carta::where('elemento','!=','5')->get();
//SELECT * from productos WHERE productos.cod_producto NOT IN (SELECT cod_producto from archivos)
	}
	
	
	protected function getMazo()
	{
		
		
		$id = Auth::user()->id;
  
        return DB::select(DB::raw('call getMazo('.$id.',1)'));
	}
	
	
	protected function getMazoJuego()
	{
		
		
		$id = Auth::user()->id;
		
	return Carta::join('mazo','carta.id','=','mazo.id_carta')
	
	       ->where('mazo.id_jug','=',$id)
	       ->where('mazo.estatus','=',1)
	       ->orderByRaw("RAND()")->get();
	}
	
	protected function getAlmacen()
	{
		
		
		$id = Auth::user()->id;
  
        return DB::select(DB::raw('call getMazo('.$id.',0)'));
		
		
	}
	
	protected function getMons(){
		
		
		return Carta::where('elemento','!=','5')->get();
		
		
		
	}
		protected function getMagica(){
		
		
		return Carta::where('elemento','=','5')->get();
		
		
		
	}
	
	
	public function get($id)
    {
        return Carta::find($id);
    }
    

    
     public function enviarAlmacen($cartas)
   {
   	
      $aux=json_decode($cartas, true);
   	  $id = Auth::user()->id;
   	  
   	  foreach ($aux as $c)
    {
    	
    	DB::table('mazo')
        ->where('id_carta',$c['id'])
        ->where('id_jug',$id)
        ->update(array('estatus' => 0));
    	
    }
    
   	  
   }
   
   public function enviarMazo($cartas)
   {
   	
   	  $aux=json_decode($cartas, true);
   	  $id = Auth::user()->id;
   	  
   	  foreach ($aux as $c)
    {
    	
    	DB::table('mazo')
        ->where('id_carta', $c['id'])
        ->where('id_jug', $id)
        ->update(array('estatus' => 1));
    	
    }
   	  
   }
    
    public function comprarCartas($cartas)
   {
   	  //echo "<script type='text/javascript'>alert('$message');</script>";
   	  $aux=json_decode($cartas, true);
   	  $id = Auth::user()->id;
   	  $precios=0;
   	  
   	foreach ($aux as $d)
    {
     $carta=Carta::find($d['id']);
     $precios = $precios + $carta->precio;
    }
   
    $usuario = Usuario::find($id);
    if ($precios <=$usuario->oro){
        
    
   	    
        foreach ($aux as $c)
        {
        	
        	$carta =  Carta::find($c['id']);
        
        		//DB::table('usuario')
        	   //->where('id', $id)
        	   //->update(array('oro' =>$usuario->oro-$carta->precio));
        		
        		$mazo = new Mazo;
        		$mazo->id_jug=$id;
        		$mazo->id_carta=$c['id'];
        		$mazo->estatus=0;
        		$mazo->save();
        		$usuario->oro-=$carta->precio;
        		$usuario->save();
        	
        }
        
       return 1; 
    }
    
    else{
     
     
     return 0;
     
    }
    	
    }
     
    
    
}