<?php

class ActividadController extends Controller
{
    
    
    protected function getAll(){
        
        
        return Actividad::all();
        
    }
    
    protected function getUserAct(){
        
        
        $id = Auth::user()->id;
        
        
        return Actividad::where('actividad.id_user','=',$id)
        
        ->orWhere(function($query){
            
           
           $query->where('actividad.id_user','=',0); 
            
        })
        
		->orderBy('id', 'DESC')	
			
        ->get();
        
    }
	   
    
	protected function insertarActividad($post1){
		
		$actividad = new Actividad;
	    $id = Auth::user()->id;
	
		
		$data=array(
                'descripcion'=>$post1,
                'id_user'=>$id,
                  );
			
		$actividad->fill($data);

	    $actividad->save();
		
	
		
		
	}
	
    
    
}


?>