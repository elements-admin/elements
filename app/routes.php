<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	 if(Auth::user())
    {
        return Redirect::to('home');
    }
	return View::make('principal/login');
});


Route::get('home', function()
{
     if(Auth::user())
    {
        return View::make('principal/home');
    }
    return Redirect::to('/')->with('mensaje',true);;
});

Route::get('acercade', function()
{
     if(Auth::user())
    {
        return View::make('principal/acercade');
    }
    return Redirect::to('/')->with('mensaje',true);;
});

Route::get('test', function()
{
     if(Auth::user())
    {
        return View::make('test');
    }
    return Redirect::to('/')->with('mensaje',true);;
});

Route::get('store', function()
{
     if(Auth::user())
    {
        return View::make('principal/tienda');
    }
    return Redirect::to('/')->with('mensaje',true);;
});

Route::get('historia', function()
{
     if(Auth::user())
    {
        return View::make('principal/historia');
    }
    return Redirect::to('/')->with('mensaje',true);;
});

Route::get('mazo', function()
{
     if(Auth::user())
    {
        return View::make('principal/mazo');
    }
    return Redirect::to('/')->with('mensaje',true);;
});

Route::post('game', function()
{
     if(Auth::user())
    {
        return View::make('juego/principal');
    }
    return Redirect::to('/')->with('mensaje',true);;
});

Route::get('nuevoUsuario', function()
{
    //$cartas=Carta::orderBy(DB::raw('RAND()')->get()->take(40));
    $elements =Elemento::all()->take(4);
	return View::make('principal/nuevoUsuario')->with('elements',$elements);
});

Route::get('enviarAlmacen/{cartas}', array('uses'=>'CartasController@enviarAlmacen'));
Route::get('enviarMazo/{cartas}', array('uses'=>'CartasController@enviarMazo'));
Route::get('comprarCartas/{cartas}', array('uses'=>'CartasController@comprarCartas'));
Route::get('logOut', array('uses'=>'LoginController@logOut'));


Route::post('insActividad/{msj}', array('uses'=>'ActividadController@insertarActividad'));
Route::post('login','LoginController@post_index');
Route::post('nuevoUsuario','UsuariosController@crearUsuario');
Route::post('actPerfil/{bandera}', array('uses'=>'UsuariosController@actualizarPerfil'));
Route::get('insertarPartida/{jug2}', array('uses'=>'PartidaController@insertarPartida'));

Route::post('modificarUsuario','UsuariosController@modificarUsuario');

Route::get('getCorreo/{nick}', 'UsuariosController@getCorreo');
Route::get('getClave/{nick}', 'UsuariosController@getClave');

Route::resource('getElementos', 'ElementosController@getAll');

Route::resource('getCartas', 'CartasController@getAll');

Route::resource('getMazo', 'CartasController@getMazo');

Route::resource('getAlmacen', 'CartasController@getAlmacen');
 
Route::resource('getInfoUser','UsuariosController@getInfoUser');

Route::resource('getActividad','ActividadController@getUserAct');

Route::resource('getMonstruo','CartasController@getMons');

Route::resource('getMagicas', 'CartasController@getMagica');

Route::resource('getMazoJuego','CartasController@getMazoJuego');

