<?php

class Partida extends Eloquent
{ //Todos los modelos deben extender la clase Eloquent
    
    protected $table = 'partida';
    protected $fillable= array('id_jugador1','id_jugador2','id_ganador'); 
    protected $hidden = array('updated_at','created_at');
    public $timestamps = false; 
}
   

?>