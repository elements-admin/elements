<?php

class Mazo extends Eloquent
{ //Todos los modelos deben extender la clase Eloquent
    protected $table = 'mazo'; 
    protected $hidden = array('updated_at','created_at');
    protected $fillable = array('id_jug','id_carta','estatus');
    public $timestamps = false; 
}
?>