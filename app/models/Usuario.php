<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Usuario extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
     protected $fillable = array('user', 'nombre','password','correo','id_elemento');
	 
	protected $table = 'usuario';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
   
	public function isValid($data)
    {
        $rules = array(
            'nombre' => 'required|min:4|max:40',
            'user' => 'required|min:4|max:10|unique:usuario',
            'correo' => 'required|min:4|max:40|email|unique:usuario',
            'password' => 'required|min:4|max:40',
            'id_elemento'=>'required'
        );

        $validator = Validator::make($data, $rules);

        if ($validator->passes())
        {
            return true;
        }

        $this->errors = $validator->errors();

        return false;
    }
    
    
    
    public function isValidModificar($data)
    {
        $rules = array(
            'nombre'   => 'required|min:4|max:40',
            'correo'   => 'required|min:4|max:40|email|unique:usuario',
            'password' => 'required|min:4|max:40',
            
        );

        $validator = Validator::make($data, $rules);

        if ($validator->passes())
        {
            return true;
        }

        $this->errors = $validator->errors();

        return false;
    }
    
      public function validarCorreo($data)
    {
        $rules = array(
            'correo'   =>   'required|min:4|max:40|email|unique:usuario'
        );

        $validator = Validator::make($data, $rules);

        if ($validator->passes())
        {
            return true;
        }

        $this->errors = $validator->errors();

        return false;
    }

    public static  function getModelo()
    {
        return array(
            'user' =>'',
            'nombre' => '',
            'password' => '',
            'correo' => '',
            'nivel' => '',
            'experiencia' => '',
            'id_elemento'=>''
        );
    }
}
