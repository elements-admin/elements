<?php
class Elemento extends Eloquent
{ //Todos los modelos deben extender la clase Eloquent
    protected $fillable = array('id','descripcion','ruta_logo','nombre');
    protected $table = 'elemento';
    protected $hidden = array('updated_at');
}
?>